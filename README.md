# Sample work explanations 

## C# work example 
[CS-example](https://gitlab.fit.cvut.cz/horenjak/sample-work/blob/master/CS-example "CS-example") CS-example contains code that I wrote in high school.It is written in C# in visual studios windows forms.And it solves simple optimalization tasks using simplex methods. It is an older code, and I learned a lot since then.  

## C work example 
[hw_05_goods_sale_c_example](https://gitlab.fit.cvut.cz/horenjak/sample-work/blob/master/hw_05_goods_sale_c_example "hw_05_goods_sale_c_example") is a school homework form 2021. The assignment was to make a code that would go through made purchases and returned the most sold goods. 

## C++ work example 

[Cpp-example](https://gitlab.fit.cvut.cz/horenjak/sample-work/blob/master/Cpp-example "Cpp-example") again contains one of the many homework’s that I had to do in second semester of university in 2021. The task was to implement class Cpolynomial which will represent polynomial and overload operators.

[pacman_cpp_example](https://gitlab.fit.cvut.cz/horenjak/sample-work/blob/master/pacman_cpp_example "pacman_cpp_example") this is my semestral work from 2021. The goal was to make a functioning game of pacman. 

[IOT_watering_system](https://gitlab.fit.cvut.cz/horenjak/sample-work/blob/master/IOT_watering_system "IOT_watering_system") is my latest project. I worked on it with my teammate Rhodon van Tilburg while I was at Utrecht University. The assignment was to make a smart plant watering system. 