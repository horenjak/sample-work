#line 1 "/Users/jakubhorenin/interactiontech-assignment-2/src/sensors/temp_pressure.cpp"
#include "temp_pressure.h"

TempPressure::TempPressure() {}

void TempPressure::init() {
  // Wire.begin is already called for the display
  connected = bmp280.begin(BMP280_ADDRESS_ALT, BMP280_CHIPID);
  bmp280.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
                     Adafruit_BMP280::SAMPLING_X8,     /* Temp. oversampling */
                     Adafruit_BMP280::SAMPLING_X16,    /* Pressure oversampling */
                     Adafruit_BMP280::FILTER_X4,       /* Filtering. */
                     Adafruit_BMP280::STANDBY_MS_250); /* Standby time. */
}

float TempPressure::getTemperature() {
  return bmp280.readTemperature();
}

float TempPressure::getPressure() {
  return bmp280.readPressure();
}

void TempPressure::printTemperature(){
  Serial.print(F("Temperature = "));
  Serial.print(getTemperature());
  Serial.println(" *C");
}

void TempPressure::printPressure(){
  Serial.print(F("Pressure = "));
  Serial.print(getPressure());
  Serial.println(" Pa");
}