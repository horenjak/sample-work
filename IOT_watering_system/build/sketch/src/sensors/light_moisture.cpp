#line 1 "/Users/jakubhorenin/interactiontech-assignment-2/src/sensors/light_moisture.cpp"
#include "light_moisture.h"

bool senseMoistureTimer(int self) {
  reinterpret_cast<LightMoisture*>(self)->senseMoisture();
  return true;
}

bool senseLight(int arg) {
  LightMoisture* self = reinterpret_cast<LightMoisture*>(arg);
  self->moistureSensor = false;
  digitalWrite(self->pinOutput, LOW);
  return false;
}

bool updateTimer(int self) {
  reinterpret_cast<LightMoisture*>(self)->read();
  return true;
}

bool enableSensor(int self) {
  reinterpret_cast<LightMoisture*>(self)->sensorEnabled = true;
  return false;
}

LightMoisture::LightMoisture(uint8_t inputPin, uint8_t outputPin, TIMER& timer):
pinInput{inputPin},
pinOutput{outputPin},
timer{timer}
{
  pinMode(pinInput, INPUT);
  pinMode(pinOutput, OUTPUT);
  timer.every(senseMoistureInterval, senseMoistureTimer, reinterpret_cast<int>(this));
  timer.every(sensorReadInterval, updateTimer, reinterpret_cast<int>(this));
}

void LightMoisture::read() {
  if (!sensorEnabled)
    return;

  if(moistureSensor)
    moistureAverage.addValue(analogRead(pinInput));
  else
    lightAverage.addValue(analogRead(pinInput));
}

void LightMoisture::senseMoisture() {
  if (moistureSensor)
    return;
  
  moistureSensor = true;
  sensorEnabled = false;
  digitalWrite(pinOutput, HIGH);
  timer.in(moistureStabilizeTime, enableSensor, reinterpret_cast<int>(this));
  timer.in(moistureStabilizeTime + moistureReadTime, senseLight, reinterpret_cast<int>(this));
}

bool LightMoisture::tryGetCurrentLightValueAndReset(int& value) {
  if (lightAverage.hasValue()) {
    value = lightAverage.getAverage();
    lightAverage.reset();
    return true;
  }
  return false;
}

bool LightMoisture::tryGetCurrentMoistureValueAndReset(int& value) {
  if (moistureAverage.hasValue()) {
    value = moistureAverage.getAverage();
    moistureAverage.reset();
    return true;
  }
  return false;
}

void LightMoisture::printLightSensor(){
  if (!lightAverage.hasValue())
    return;
  Serial.print("Light Sensor: ");
  Serial.println(lightAverage.getAverage());
}

void LightMoisture::printMoistureSensor(){
  if (!moistureAverage.hasValue())
    return;
  Serial.print("Moisture Sensor: ");
  Serial.println(moistureAverage.getAverage());
}