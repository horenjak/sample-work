#line 1 "/Users/jakubhorenin/interactiontech-assignment-2/src/connectivity/wifi-controller.h"
#ifndef Wifi_H_
#define Wifi_H_

#include "mqtt.h"

#include <WiFiManager.h>
#include <ESP_EEPROM.h>

#define HARDCODE_WIFI_SETTINGS true
#define HARDCODED_SSID "Kubik"
#define HARDCODED_PASSWORD "g123heslo"
#define HARDCODED_MQTT_CONFIG (MQTTConfig){"mqtt.uu.nl", "infob3it/081/", "Rose", "student081", "Tcj3PhSm"}

class WifiController
{
private:
  static constexpr char accessPointSsid[] = "ESP-PlantWater";
  static WifiController& controller;
  WiFiManager manager;
  WiFiManagerParameter mqttServer = WiFiManagerParameter("mqttS", "MQTT Server"),
                       mqttTopicPrefix = WiFiManagerParameter("mqttT", "MQTT Topic Prefix"),
                       mqttPlantName = WiFiManagerParameter("mqttPl", "MQTT Plant Name"),
                       mqttUsername = WiFiManagerParameter("mqttU", "MQTT Username"),
                       mqttPassword = WiFiManagerParameter("mqttP", "MQTT Password");
  MQTT& mqtt;
  void setParameters();
  void connected();
public:
  WifiController(MQTT& mqtt);
  void init();
  void update();
  void reset();
};

#endif