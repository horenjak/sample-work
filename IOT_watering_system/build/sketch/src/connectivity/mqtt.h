#line 1 "/Users/jakubhorenin/interactiontech-assignment-2/src/connectivity/mqtt.h"
#ifndef Mqtt_H_
#define Mqtt_H_

#include <ESP8266WiFi.h>
// Please note that this is a changed version of the library that supports the retain flag on published packages
#include "Adafruit_MQTT_Library/Adafruit_MQTT.h"
#include "Adafruit_MQTT_Library/Adafruit_MQTT_Client.h"

#define MQTT_MAX_SERVER_LENGTH 20
#define MQTT_MAX_TOPIC_PREFIX_LENGTH 15
#define MQTT_MAX_PLANT_NAME_LENGTH 12
#define MQTT_MAX_USERNAME_LENGTH 15
#define MQTT_MAX_PASSWORD_LENGTH 25

#define MQTT_MAX_TOPIC_LENGTH 40
#define MQTT_MAX_CLIENT_ID_LENGTH 10

struct MQTTConfig {
  char server[MQTT_MAX_SERVER_LENGTH];
  char topicPrefix[MQTT_MAX_TOPIC_PREFIX_LENGTH];
  char plantName[MQTT_MAX_PLANT_NAME_LENGTH];
  char username[MQTT_MAX_USERNAME_LENGTH];
  char password[MQTT_MAX_PASSWORD_LENGTH];
};

class MQTT_Client: public Adafruit_MQTT_Client
{
private:
  WiFiClient client;
protected:
  uint16_t readPacket(uint8_t *buffer, uint16_t maxlen, int16_t timeout) override;
public:
  MQTT_Client(const char *server, uint16_t port,
                const char *cid, const char *user, const char *pass);
  String getConnectionString();
};

class MQTT
{
private:
  static const unsigned long CONNECTION_RETRY_TIMEOUT = 5000;
  static const uint16_t PORT = 1883;
  char clientId[MQTT_MAX_CLIENT_ID_LENGTH];
  char statusTopic[MQTT_MAX_TOPIC_LENGTH], waterTopic[MQTT_MAX_TOPIC_LENGTH],
       senseTopic[MQTT_MAX_TOPIC_LENGTH], targetTopic[MQTT_MAX_TOPIC_LENGTH],
       temperatureTopic[MQTT_MAX_TOPIC_LENGTH], pressureTopic[MQTT_MAX_TOPIC_LENGTH],
       lightTopic[MQTT_MAX_TOPIC_LENGTH], moistureTopic[MQTT_MAX_TOPIC_LENGTH];
  unsigned long lastConnect;
  int& moistureThreshold;
  MQTTConfig config;
  MQTT_Client* client;
  Adafruit_MQTT_Subscribe* waterSubscription, *senseSubscription, *targetSubscription;
  void (*waterCallback)(uint16_t amount);
  void (*senseCallback)(char* sensor);

  inline const char* getTopic(String topicName, char* resultTopic);
  inline const char* getPlantTopic(const __FlashStringHelper* topicName, char* resultTopic);
  void connect();
  void publish(const char* topic, const char* value, bool manual);
  void publish(const char* topic, float value, bool manual);
  void publish(const char* topic, int value, bool manual);
public:
  MQTT(int& waterThreshold);
  void init();
  void update();
  MQTTConfig& getConfig();
  void setWaterCallback(void (*waterCallback)(uint16_t amount));
  void setSenseCallback(void (*senseCallback)(char* sensor));

  void publishTemperature(float value, bool manual = false);
  void publishPressure(float value, bool manual = false);
  void publishLight(int value, bool manual = false);
  void publishMoisture(int value, bool manual = false);
};

#endif