#line 1 "/Users/jakubhorenin/interactiontech-assignment-2/src/connectivity/wifi-controller.cpp"
#include "wifi-controller.h"

WifiController::WifiController(MQTT& mqtt): mqtt {mqtt} {}

bool shouldSaveConfig = false;

void saveConfig() {
  shouldSaveConfig = true;
}

void WifiController::init() {
  manager.setConfigPortalBlocking(false);
  EEPROM.begin(sizeof(MQTTConfig));

  MQTTConfig& mqttConfig = mqtt.getConfig();
  #if HARDCODE_WIFI_SETTINGS
  manager.preloadWiFi(HARDCODED_SSID, HARDCODED_PASSWORD);
  mqttConfig = HARDCODED_MQTT_CONFIG;

  if (!manager.getWiFiIsSaved()) {
    WiFi.persistent(true);
    WiFi.begin(HARDCODED_SSID, HARDCODED_PASSWORD, 0, NULL, false);
    Serial.println("Wrote hardcoded wifi settings");
  }
  #else
  if (EEPROM.percentUsed() == -1) {
    // No data yet
    Serial.println(F("No MQTT config found"));
    if (manager.getWiFiIsSaved())
      manager.resetSettings();
  } else
    EEPROM.get(0, mqttConfig);
  #endif

  setParameters();

  manager.addParameter(&mqttServer);
  manager.addParameter(&mqttTopicPrefix);
  manager.addParameter(&mqttPlantName);
  manager.addParameter(&mqttUsername);
  manager.addParameter(&mqttPassword);

  manager.setSaveConfigCallback(saveConfig);

  String password = F("Plant-");
  password += ESP.getFlashChipId();

  if (manager.autoConnect(accessPointSsid, password.c_str()))
    connected(); // Connected from stored wifi settings
  else { // Connection failed, starting Access Point
    Serial.print(F("Started configuration portal with password '"));
    Serial.print(password);
    Serial.println("'");
  }
}

void WifiController::setParameters() {
  MQTTConfig& mqttConfig = mqtt.getConfig();
  mqttServer.setValue(mqttConfig.server, MQTT_MAX_SERVER_LENGTH);
  mqttTopicPrefix.setValue(mqttConfig.topicPrefix, MQTT_MAX_TOPIC_PREFIX_LENGTH);
  mqttPlantName.setValue(mqttConfig.plantName, MQTT_MAX_PLANT_NAME_LENGTH);
  mqttUsername.setValue(mqttConfig.username, MQTT_MAX_USERNAME_LENGTH);
  mqttPassword.setValue(mqttConfig.password, MQTT_MAX_PASSWORD_LENGTH);
}

void WifiController::connected() {
  Serial.println(F("Connected to WiFi!"));

  mqtt.init();
}

void WifiController::update() {
  if (manager.getConfigPortalActive()) {
    bool processResult = manager.process();
    if ((processResult || !manager.getConfigPortalActive()) && shouldSaveConfig) {
      MQTTConfig& mqttConfig = mqtt.getConfig();

      shouldSaveConfig = false;
      strlcpy(mqttConfig.server, mqttServer.getValue(), MQTT_MAX_SERVER_LENGTH);
      strlcpy(mqttConfig.topicPrefix, mqttTopicPrefix.getValue(), MQTT_MAX_TOPIC_PREFIX_LENGTH);
      strlcpy(mqttConfig.plantName, mqttPlantName.getValue(), MQTT_MAX_PLANT_NAME_LENGTH);
      strlcpy(mqttConfig.username, mqttUsername.getValue(), MQTT_MAX_USERNAME_LENGTH);
      strlcpy(mqttConfig.password, mqttPassword.getValue(), MQTT_MAX_PASSWORD_LENGTH);

      delay(0); // Yield to the WiFi module in case this step takes a while
      EEPROM.put(0, mqttConfig);
      EEPROM.commit();

      Serial.print(F("Saved configuration for MQTT Server "));
      Serial.println(mqttConfig.server);
    }

    if (processResult)
      connected();
    
    return;
  }

  mqtt.update();
}

void WifiController::reset() {
  if (manager.getConfigPortalActive())
    return;
  
  String password = F("Plant-");
  password += ESP.getFlashChipId();

  manager.setBreakAfterConfig(true);
  manager.startConfigPortal(accessPointSsid, password.c_str());
}