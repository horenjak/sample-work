#line 1 "/Users/jakubhorenin/interactiontech-assignment-2/src/servo.h"
#ifndef Servo_H_
#define Servo_H_

#include <Servo.h>

class ServoController
{
private:
  const float TARGET_SPEED = 30; // In degrees per second
  const double MAX_STEP_SIZE = 5; // The maximum degrees to change per tick
                          // (fail-safe for when the update method doesn't get called often enough)
  Servo servo;
  uint8_t pin;
  int targetPos;
  float leftOverMovement = 0;
  unsigned long lastUpdate;
public:
  ServoController(uint8_t pin, int initialPosition);
  void move(int position);
  void update();
  bool isAtTarget();
  int currentPosition();
  int targetPosition();
};

#endif