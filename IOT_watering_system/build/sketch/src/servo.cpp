#line 1 "/Users/jakubhorenin/interactiontech-assignment-2/src/servo.cpp"
#include "servo.h"

#include "util.h"

ServoController::ServoController(uint8_t pin, int initialPosition):
  pin {pin}, lastUpdate {millis()}, targetPos {initialPosition} {
  // Without this, each servo.write takes 20 ms, as explained at https://github.com/esp8266/Arduino/issues/8081
  enablePhaseLockedWaveform();
  servo.write(initialPosition);
  servo.attach(pin);
}

void ServoController::move(int position) {
  targetPos = position;
  lastUpdate = millis();
  leftOverMovement = 0;
}

void ServoController::update() {
  int currentPos = servo.read();
  if (currentPos == targetPos) {
    lastUpdate = millis();
    return;
  }

  unsigned long time = millis();
  float movement = SIGN(targetPos - currentPos) * min(TARGET_SPEED * (time - lastUpdate) * 0.001, MAX_STEP_SIZE) + leftOverMovement;
  int truncMove = movement;
  if (truncMove == 0)
    return; // Don't update the lastUpdate until we move
  
  leftOverMovement = movement - truncMove;
  servo.write(currentPos + truncMove);
  lastUpdate = time;
}

bool ServoController::isAtTarget() {
  return servo.read() == targetPos;
}

int ServoController::currentPosition() {
  return servo.read();
}

int ServoController::targetPosition() {
  return targetPos;
}