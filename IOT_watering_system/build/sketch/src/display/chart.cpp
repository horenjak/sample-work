#line 1 "/Users/jakubhorenin/interactiontech-assignment-2/src/display/chart.cpp"
#include "chart.h"

void OLED_Chart::init(OLEDDisplay* display) {
  this->display = display;
}

void OLED_Chart::drawChart()
{
    double i, temp;
    _dig = 0;
    _previous_x_coordinate = _x_lower_left_coordinate;
    _previous_y_coordinate = _y_lower_left_coordinate;
    _actual_x_coordinate = _x_lower_left_coordinate;
    _x_drawing_offset = 0;

    if (_y_labels_visible)
    {
      int16_t x, y;
      uint16_t w, h;

      display->setFont(ArialMT_Plain_10);
      display->setColor(WHITE);
      _x_drawing_offset = display->getStringWidth(_y_max_label);

      // high label
      display->drawString(_x_lower_left_coordinate, _y_lower_left_coordinate - _chart_height - 5, _y_max_label);
      // low label
      display->drawString(_x_lower_left_coordinate, _y_lower_left_coordinate - 10, _y_min_label);

      if (w > _x_drawing_offset)
      {
          _x_drawing_offset = w;
      }

      // compensation for the y axis tick lines
      _x_drawing_offset += 4;
    }
    // draw y divisions
    for (i = _y_lower_left_coordinate; i <= _y_lower_left_coordinate + _chart_height; i += _yinc_div)
    {
        temp = (i - _y_lower_left_coordinate) * (_y_lower_left_coordinate - _chart_height - _y_lower_left_coordinate) / (_chart_height) + _y_lower_left_coordinate;
        if (i == _y_lower_left_coordinate)
          display->drawHorizontalLine(_x_lower_left_coordinate - 3 + _x_drawing_offset, temp, _chart_width + 3 - _x_drawing_offset);
        else
          display->drawHorizontalLine(_x_lower_left_coordinate - 3 + _x_drawing_offset, temp, 3);
    }
    // draw x divisions
    for (i = 0; i <= _chart_width - _x_drawing_offset; i += _xinc_div)
    {
        temp = (i) + _x_lower_left_coordinate + _x_drawing_offset;
        if (i == 0)
          display->drawVerticalLine(temp, _y_lower_left_coordinate - _chart_height, _chart_height + 3);
        else
          display->drawVerticalLine(temp, _y_lower_left_coordinate, 3);
    }
}

void OLED_Chart::setYLimits(double ylo, double yhi)
{
  _y_min_values = ylo;
  _y_max_values = yhi;
}

void OLED_Chart::setYLimitLabels(char *loLabel, char *hiLabel)
{
  _y_min_label = loLabel;
  _y_max_label = hiLabel;
}

void OLED_Chart::setYLabelsVisible(bool yLabelsVisible)
{
    _y_labels_visible = yLabelsVisible;
}

void OLED_Chart::setMidLineVisible(bool lineVisible)
{
    _mid_line_visible = lineVisible;
}

void OLED_Chart::setPointGeometry(char pointGeometry)
{
  _point_geometry = pointGeometry;
}

void OLED_Chart::setChartCoordinates(double x, double y)
{
  _x_lower_left_coordinate = x;
  _y_lower_left_coordinate = y;
}

void OLED_Chart::setChartWidthAndHeight(double w, double h)
{
  _chart_width = w;
  _chart_height = h;
}

void OLED_Chart::setAxisDivisionsInc(double xinc, double yinc)
{
  _xinc_div = xinc;
  _yinc_div = yinc;
}

void OLED_Chart::setXIncrement(double xinc)
{
  _x_inc = xinc;
}

bool OLED_Chart::updateChart(double value)
{
  if (_actual_x_coordinate >= _x_lower_left_coordinate + _chart_width - _x_drawing_offset)
      return false;

  if (value < _y_min_values)
      value = _y_min_values;

  if (value > _y_max_values)
      value = _y_max_values;

  double y = (value - _y_min_values) * (_y_lower_left_coordinate - _chart_height - _y_lower_left_coordinate) / (_y_max_values - _y_min_values) + _y_lower_left_coordinate;
  display->drawLine(_previous_x_coordinate + _x_drawing_offset, _previous_y_coordinate, _actual_x_coordinate + _x_drawing_offset, y);

  _previous_x_coordinate = _actual_x_coordinate;
  _previous_y_coordinate = y;

  _actual_x_coordinate += _x_inc;

  if (_point_geometry == POINT_GEOMETRY_CIRCLE)
    display->fillCircle(_previous_x_coordinate + _x_drawing_offset, _previous_y_coordinate, 2);
  return true;
}