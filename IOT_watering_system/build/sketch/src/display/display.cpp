#line 1 "/Users/jakubhorenin/interactiontech-assignment-2/src/display/display.cpp"
#include "display.h"

Display::Display(uint8_t sdaPin, uint8_t sclPin, Screen** startScreens, uint8_t screenCount):
  display{0x3c, sdaPin, sclPin}, screen {startScreens[0]}, defaultScreens {startScreens},
  defaultScreenCount {screenCount}, screenIndex {0} {}

void Display::init() {
  display.init();
  display.flipScreenVertically();
  screen->redraw(display);
}

void Display::update() {
  screen->update(display);
}

void Display::setScreen(Screen& screen) {
  this->screen = &screen;
  screen.redraw(display);
}

void Display::nextScreen() {
  if (++screenIndex >= defaultScreenCount)
    screenIndex = 0;
  setScreen(*(defaultScreens[screenIndex]));
}

OLEDDisplay& Display::getDisplay() {
  return display;
}