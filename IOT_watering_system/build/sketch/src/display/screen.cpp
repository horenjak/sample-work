#line 1 "/Users/jakubhorenin/interactiontech-assignment-2/src/display/screen.cpp"
#include "screen.h"
#include "images.h"

ChartScreen::ChartScreen(double minValue, double maxValue):
  chart {}, minValue {minValue}, maxValue {maxValue}, drawIndex {0} {}

void ChartScreen::init(OLEDDisplay& display, uint8_t displayedPoints) {
  chart.init(&display);
  chart.setChartCoordinates(0, 60);                              //Chart lower left coordinates (X, Y)
  double width = 123;
  chart.setChartWidthAndHeight(width, 55);                       //Chart width = 123 and height = 60
  chart.setXIncrement(width / max(displayedPoints, data.capacity));
  chart.setYLimits(minValue, maxValue);
  char minLabel[5];
  char maxLabel[5];
  chart.setYLimitLabels(itoa(minValue, minLabel, 10), itoa(maxValue, maxLabel, 10)); //Setting Y axis labels
  chart.setYLabelsVisible(true);
  chart.setAxisDivisionsInc(12, 6);
}

void ChartScreen::add(double value) {
  if (data.isFull() && drawIndex > 0)
    drawIndex--;
  data.push(value);
}

void ChartScreen::update(OLEDDisplay& display) {
  if (drawIndex >= data.size())
    return;
  
  do {
    if (!chart.updateChart(data[drawIndex]))
      redrawInternal(display); // If the data doesn't fit on the display anymore, redraw
  } while (++drawIndex < data.size());
  display.display();
}

void ChartScreen::redrawInternal(OLEDDisplay& display) {
  display.clear();
  chart.drawChart();
  for (uint8_t i = 0; i < data.size(); i++)
    chart.updateChart(data[i]);
  drawIndex = data.size();
}

void ChartScreen::redraw(OLEDDisplay& display) {
  redrawInternal(display);
  display.display();
}
  
void ProgressScreen::updateProgress(uint8_t progress) {
  currentProgress = progress;
}

uint8_t ProgressScreen::getProgress() {
  return currentProgress;
}

void MoistureScreen::redraw(OLEDDisplay& display){
  unsigned int timePassed = millis() - timeOfWatering;
  unsigned int days = (((timePassed/1000)/60)/60)/24;
  unsigned int hours = ((((timePassed - (days * 24 * 60 * 60 * 1000))/1000)/60)/60) ;
  unsigned int mins  = ((timePassed - (hours * 60 * 60 * 1000))/1000)/60;
  display.clear();
  display.drawString(14, 10, F("Moisture level: "));
  display.drawString(90, 10, String(currentProgress) + F(" %"));
  display.drawProgressBar(14, 25, 100, 10, currentProgress);

  display.drawString(10, 40, F("Time since last watering: "));
  char timeString[18];
  display.drawStringf(25, 50, timeString, F("%0.2ddays %0.2d:%0.2dmin"), days, hours, mins);
  display.display();
}

void MoistureScreen::update(OLEDDisplay& display) {}

void MoistureScreen::updateMoisture(int moisture){
  updateProgress(max((moisture - 100) / (9 - 1), 0));
}

void MoistureScreen::resetWateringTimer(){
  timeOfWatering = millis();
}

void WateringScreen::redraw(OLEDDisplay& display){
  display.clear();
  update(display);
  display.drawXbm(50, 8, drop_width, drop_height, drop_bits);
  display.display();
}

void WateringScreen::update(OLEDDisplay& display) {
  if (isComplete)
    display.drawString(14, 53, F("Watering done!"));
  else
    display.drawProgressBar(14, 53, 100, 7, currentProgress);
  display.display();
}

void WateringScreen::complete(OLEDDisplay& display) {
  isComplete = true;
  redraw(display);
}

void WateringScreen::reset() {
  updateProgress(0);
  isComplete = false;
}