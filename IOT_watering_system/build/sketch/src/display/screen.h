#line 1 "/Users/jakubhorenin/interactiontech-assignment-2/src/display/screen.h"
#ifndef Window_H_
#define Window_H_

#include <Wire.h>
#include <CircularBuffer.h>

#include "SSD1306Wire.h"
#include "../sensors/temp_pressure.h"
#include "chart.h"

class Screen
{
private:
public:
  virtual void update(OLEDDisplay& display) = 0;
  virtual void redraw(OLEDDisplay& display) = 0;
};

class ChartScreen: public Screen
{
private:
  OLED_Chart chart;
  const double minValue, maxValue;
  CircularBuffer<double, 20> data;
  uint8_t drawIndex;
  
  void redrawInternal(OLEDDisplay& display);
public:
  ChartScreen(double min, double max);
  void init(OLEDDisplay& display, uint8_t displayedPoints);
  void add(double value);
  void update(OLEDDisplay& display) override;
  void redraw(OLEDDisplay& display) override;
};

class ProgressScreen: public Screen
{
  private:
    unsigned int timeOfWatering;
  protected:
    uint8_t currentProgress = 0;
  public:
    void updateProgress(uint8_t progress);
    uint8_t getProgress();
};

class MoistureScreen: public ProgressScreen
{
  private:
  unsigned int timeOfWatering;
  public:
    void updateMoisture(int moisture);
    void update(OLEDDisplay& display) override;
    void redraw(OLEDDisplay& display) override;
    void resetWateringTimer();
};

class WateringScreen: public ProgressScreen
{
  private:
    bool isComplete = false;
  public:
    void update(OLEDDisplay& display) override;
    void redraw(OLEDDisplay& display) override;
    void complete(OLEDDisplay& display);
    void reset();
};

#endif