# 1 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino"
# 2 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 2
# 3 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 2
# 4 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 2
# 5 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 2
# 6 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 2
# 7 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 2

# 9 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 2

constexpr unsigned long idleDisplaySwitchTime = 10000, checkAutomaticWateringTime = 5 * 60 * 1000;

Button2 button;
Timer<10, millis, int> timer;

constexpr uint8_t BUTTON_PIN = D3;
TempPressure tempPressure;
LightMoisture lightMoisture(A0, D4, timer);
ServoController servo(D5, 0); // Start at 0 degrees
ChartScreen temperatureScreen(20, 25), pressureScreen(100, 105);
MoistureScreen moistureScreen;
WateringScreen wateringScreen;
Screen *screens[] = {
  &temperatureScreen, &moistureScreen, &pressureScreen
};
Display display(D6, D7, screens, 3);

Average averageTemperature, averagePressure;
uintptr_t idleDisplayTimer, waterPlantTimer = 
# 28 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 3 4
                                             __null
# 28 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino"
                                                 , stopWateringTimer, autoWateringTimer;
uint16_t waterAmount;
int lastMoistureValue = 1024, moistureThreshold = 150;
bool automaticMode = true, moistureIsManual = false;

MQTT mqtt(moistureThreshold);
WifiController wifi(mqtt);

void setup()
{
  //progressScreen.resetWateringTimer();
  Serial.begin(115200);
  delay(1000); // Wait for Serial connection

  pinMode(16, 0x01);
  button.begin(BUTTON_PIN, 0x02);
  button.setClickHandler(switchMode);
  button.setDoubleClickHandler(waterManually);
  button.setLongClickTime(2000);
  button.setLongClickHandler(resetWifi);

  temperatureScreen.init(display.getDisplay(), 40);
  pressureScreen.init(display.getDisplay(), 50);
  display.init();
  tempPressure.init();

  mqtt.setWaterCallback(startWatering);
  mqtt.setSenseCallback(extraSense);
  wifi.init();

  timer.every(1000, updateSensors);
  timer.every(30000, publishSensors);
  idleDisplayTimer = timer.every(idleDisplaySwitchTime, nextScreen);
  autoWateringTimer = timer.every(checkAutomaticWateringTime, checkAutomaticWatering);

  lightMoisture.senseMoisture();
}

void loop()
{
  button.loop();
  servo.update();
  display.update();
  wifi.update();
  timer.tick<void>();
}

//--------- CALLBACKS ---------\

bool updateSensors(int argument) {
  float value = tempPressure.getTemperature();
  temperatureScreen.add(value);
  averageTemperature.addValue(value);

  value = tempPressure.getPressure();
  pressureScreen.add(value / 1000);
  averagePressure.addValue(value);
  return true; // Keep this timer active
}

bool publishSensors(int argument) {
  mqtt.publishTemperature(averageTemperature.getAverage());
  averageTemperature.reset();

  mqtt.publishPressure(averagePressure.getAverage());
  averagePressure.reset();

  if (lightMoisture.tryGetCurrentMoistureValueAndReset(lastMoistureValue)) {
    moistureScreen.updateMoisture(lastMoistureValue);
    mqtt.publishMoisture(lastMoistureValue, moistureIsManual);
    moistureIsManual = false;
  }

  int lightValue;
  if (lightMoisture.tryGetCurrentLightValueAndReset(lightValue))
    mqtt.publishLight(lightValue);

  return true; // Keep this timer active
}

bool nextScreen(int argument) {
  display.nextScreen();
  return true; // Keep this timer active
}

bool checkAutomaticWatering(int argument) {
  if (lastMoistureValue < moistureThreshold)
    startWatering(1000 + 5 * (moistureThreshold - lastMoistureValue));
  return true;
}

bool waterPlantServo(int argument) {
  wateringScreen.updateProgress(50 * servo.currentPosition() / servo.targetPosition());

  if (servo.isAtTarget()) {
    stopWateringTimer = timer.in(waterAmount, stopWatering);
    waterPlantTimer = timer.every(waterAmount/50, waterPlant);
    return false;
  }
  return true;
}

bool waterPlant(int argument) {
  wateringScreen.updateProgress(wateringScreen.getProgress() + 1);
  return true;
}

bool stopWatering(int argument) {
  timer.cancel(waterPlantTimer);
  waterPlantTimer = 
# 137 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 3 4
                   __null
# 137 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino"
                       ;
  stopWateringTimer = 
# 138 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 3 4
                     __null
# 138 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino"
                         ;
  idleDisplayTimer = timer.every(idleDisplaySwitchTime, nextScreen);
  wateringScreen.complete(display.getDisplay());
  servo.move(0);
  return false;
}

void startWatering(uint16_t amount) {
  moistureScreen.resetWateringTimer(); // Resets the timer since last watring
  waterAmount = amount;
  wateringScreen.reset();
  // If we 'start' watering while we are still doing the previous watering, override the current length
  if (stopWateringTimer != 
# 150 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 3 4
                          __null
# 150 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino"
                              ) {
    timer.cancel(stopWateringTimer);
    stopWateringTimer = 
# 152 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 3 4
                       __null
# 152 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino"
                           ;
  }
  if (waterPlantTimer != 
# 154 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 3 4
                        __null
# 154 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino"
                            )
    timer.cancel(waterPlantTimer);

  display.setScreen(wateringScreen);
  servo.move(130);
  timer.cancel(idleDisplayTimer);
  waterPlantTimer = timer.every(10, waterPlantServo);
}

void extraSense(char* sensor) {
  if (
# 164 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 3
     strncmp_P((
# 164 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino"
     sensor
# 164 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 3
     ), ((__extension__({static const char __pstr__[] __attribute__((__aligned__(4))) __attribute__((section( "\".irom0.pstr." "iot.ino" "." "164" "." "278" "\", \"aSM\", @progbits, 1 #"))) = (
# 164 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino"
     "temp"
# 164 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 3
     ); &__pstr__[0];}))), 0x7fffffff) 
# 164 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino"
                                    == 0)
    mqtt.publishTemperature(tempPressure.getTemperature(), true);
  else if (
# 166 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 3
          strncmp_P((
# 166 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino"
          sensor
# 166 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 3
          ), ((__extension__({static const char __pstr__[] __attribute__((__aligned__(4))) __attribute__((section( "\".irom0.pstr." "iot.ino" "." "166" "." "279" "\", \"aSM\", @progbits, 1 #"))) = (
# 166 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino"
          "press"
# 166 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 3
          ); &__pstr__[0];}))), 0x7fffffff) 
# 166 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino"
                                          == 0)
    mqtt.publishPressure(tempPressure.getPressure(), true);
  else if (
# 168 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 3
          strncmp_P((
# 168 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino"
          sensor
# 168 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 3
          ), ((__extension__({static const char __pstr__[] __attribute__((__aligned__(4))) __attribute__((section( "\".irom0.pstr." "iot.ino" "." "168" "." "280" "\", \"aSM\", @progbits, 1 #"))) = (
# 168 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino"
          "moist"
# 168 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 3
          ); &__pstr__[0];}))), 0x7fffffff) 
# 168 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino"
                                          == 0) {
    moistureIsManual = true;
    lightMoisture.senseMoisture();
  } else {
    Serial.print(((reinterpret_cast<const __FlashStringHelper *>(
# 172 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 3
                (__extension__({static const char __pstr__[] __attribute__((__aligned__(4))) __attribute__((section( "\".irom0.pstr." "iot.ino" "." "172" "." "281" "\", \"aSM\", @progbits, 1 #"))) = (
# 172 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino"
                "Unknown sensor "
# 172 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 3
                ); &__pstr__[0];}))
# 172 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino"
                ))));
    Serial.println(sensor);
  }
}


void resetWifi(Button2& button) {
  Serial.println("reset Wifi");
  wifi.reset();
}

void waterManually(Button2& button) {
  Serial.println(((reinterpret_cast<const __FlashStringHelper *>(
# 184 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 3
                (__extension__({static const char __pstr__[] __attribute__((__aligned__(4))) __attribute__((section( "\".irom0.pstr." "iot.ino" "." "184" "." "282" "\", \"aSM\", @progbits, 1 #"))) = (
# 184 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino"
                "Watering from manual button press..."
# 184 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino" 3
                ); &__pstr__[0];}))
# 184 "/Users/jakubhorenin/interactiontech-assignment-2/iot.ino"
                ))));
  startWatering(2000);
}

void switchMode(Button2& button) {
  automaticMode = !automaticMode;
  digitalWrite(16, automaticMode ? 0x0 : 0x1);
  if (automaticMode)
    autoWateringTimer = timer.every(checkAutomaticWateringTime, checkAutomaticWatering);
  else
    timer.cancel(autoWateringTimer);
}
