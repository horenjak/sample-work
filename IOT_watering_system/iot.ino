#include "src/servo.h"
#include "src/display/display.h"
#include "src/sensors/light_moisture.h"
#include "src/sensors/temp_pressure.h"
#include "src/connectivity/wifi-controller.h"
#include "src/util.h"

#include <Button2.h>

constexpr unsigned long idleDisplaySwitchTime = 10000, checkAutomaticWateringTime = 5 * 60 * 1000;

Button2 button;
TIMER timer;

constexpr uint8_t BUTTON_PIN = D3;
TempPressure tempPressure;
LightMoisture lightMoisture(A0, D4, timer);
ServoController servo(D5, 0); // Start at 0 degrees
ChartScreen temperatureScreen(20, 25), pressureScreen(100, 105);
MoistureScreen moistureScreen;
WateringScreen wateringScreen;
Screen *screens[] = {
  &temperatureScreen, &moistureScreen, &pressureScreen
};
Display display(D6, D7, screens, 3);

Average averageTemperature, averagePressure;
uintptr_t idleDisplayTimer, waterPlantTimer = NULL, stopWateringTimer, autoWateringTimer;
uint16_t waterAmount;
int lastMoistureValue = 1024, moistureThreshold = 150;
bool automaticMode = true, moistureIsManual = false;

MQTT mqtt(moistureThreshold);
WifiController wifi(mqtt);

void setup()
{
  //progressScreen.resetWateringTimer();
  Serial.begin(115200);
  delay(1000); // Wait for Serial connection

  pinMode(LED_BUILTIN, OUTPUT);
  button.begin(BUTTON_PIN, INPUT_PULLUP);
  button.setClickHandler(switchMode);
  button.setDoubleClickHandler(waterManually);
  button.setLongClickTime(2000);
  button.setLongClickHandler(resetWifi);

  temperatureScreen.init(display.getDisplay(), 40);
  pressureScreen.init(display.getDisplay(), 50);
  display.init();
  tempPressure.init();

  mqtt.setWaterCallback(startWatering);
  mqtt.setSenseCallback(extraSense);
  wifi.init();

  timer.every(1000, updateSensors);
  timer.every(30000, publishSensors);
  idleDisplayTimer = timer.every(idleDisplaySwitchTime, nextScreen);
  autoWateringTimer = timer.every(checkAutomaticWateringTime, checkAutomaticWatering);

  lightMoisture.senseMoisture();
}

void loop()
{
  button.loop();
  servo.update();
  display.update();
  wifi.update();
  timer.tick<void>();
}

//--------- CALLBACKS ---------\\

bool updateSensors(int argument) {
  float value = tempPressure.getTemperature();
  temperatureScreen.add(value);
  averageTemperature.addValue(value);

  value = tempPressure.getPressure();
  pressureScreen.add(value / 1000);
  averagePressure.addValue(value);
  return true; // Keep this timer active
}

bool publishSensors(int argument) {
  mqtt.publishTemperature(averageTemperature.getAverage());
  averageTemperature.reset();

  mqtt.publishPressure(averagePressure.getAverage());
  averagePressure.reset();

  if (lightMoisture.tryGetCurrentMoistureValueAndReset(lastMoistureValue)) {
    moistureScreen.updateMoisture(lastMoistureValue);
    mqtt.publishMoisture(lastMoistureValue, moistureIsManual);
    moistureIsManual = false;
  }

  int lightValue;  
  if (lightMoisture.tryGetCurrentLightValueAndReset(lightValue))
    mqtt.publishLight(lightValue);

  return true; // Keep this timer active
}

bool nextScreen(int argument) {
  display.nextScreen();
  return true; // Keep this timer active
}

bool checkAutomaticWatering(int argument) {
  if (lastMoistureValue < moistureThreshold)
    startWatering(1000 + 5 * (moistureThreshold - lastMoistureValue));
  return true;
}

bool waterPlantServo(int argument) {
  wateringScreen.updateProgress(50 * servo.currentPosition() / servo.targetPosition());

  if (servo.isAtTarget()) {
    stopWateringTimer = timer.in(waterAmount, stopWatering);
    waterPlantTimer = timer.every(waterAmount/50, waterPlant);
    return false;
  }
  return true;
}

bool waterPlant(int argument) {
  wateringScreen.updateProgress(wateringScreen.getProgress() + 1);
  return true;
}

bool stopWatering(int argument) {
  timer.cancel(waterPlantTimer);
  waterPlantTimer = NULL;
  stopWateringTimer = NULL;
  idleDisplayTimer = timer.every(idleDisplaySwitchTime, nextScreen);
  wateringScreen.complete(display.getDisplay());
  servo.move(0);
  return false;
}

void startWatering(uint16_t amount) {
  moistureScreen.resetWateringTimer(); // Resets the timer since last watring
  waterAmount = amount;
  wateringScreen.reset();
  // If we 'start' watering while we are still doing the previous watering, override the current length
  if (stopWateringTimer != NULL) {
    timer.cancel(stopWateringTimer);
    stopWateringTimer = NULL;
  }
  if (waterPlantTimer != NULL)
    timer.cancel(waterPlantTimer);
  
  display.setScreen(wateringScreen);
  servo.move(130);
  timer.cancel(idleDisplayTimer);
  waterPlantTimer = timer.every(10, waterPlantServo);
}

void extraSense(char* sensor) {
  if (strcmp_P(sensor, PSTR("temp")) == 0)
    mqtt.publishTemperature(tempPressure.getTemperature(), true);
  else if (strcmp_P(sensor, PSTR("press")) == 0)
    mqtt.publishPressure(tempPressure.getPressure(), true);
  else if (strcmp_P(sensor, PSTR("moist")) == 0) {
    moistureIsManual = true;
    lightMoisture.senseMoisture();
  } else {
    Serial.print(F("Unknown sensor "));
    Serial.println(sensor);
  }
}


void resetWifi(Button2& button) {
  Serial.println("reset Wifi");
  wifi.reset();
}

void waterManually(Button2& button) {
  Serial.println(F("Watering from manual button press..."));
  startWatering(2000);
}

void switchMode(Button2& button) {
  automaticMode = !automaticMode;
  digitalWrite(LED_BUILTIN, automaticMode ? LOW : HIGH);
  if (automaticMode)
    autoWateringTimer = timer.every(checkAutomaticWateringTime, checkAutomaticWatering);
  else
    timer.cancel(autoWateringTimer);
}