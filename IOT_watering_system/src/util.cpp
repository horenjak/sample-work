#include "util.h"

bool Average::hasValue() {
  return count > 0;
}

void Average::addValue(float value) {
  total += value;
  count++;
}

float Average::getAverage() {
  return total / count;
}

void Average::reset() {
  total = count = 0;
}