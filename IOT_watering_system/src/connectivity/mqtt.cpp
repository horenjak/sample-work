#include "mqtt.h"

MQTT::MQTT(int& waterThreshold): config {"mqtt.uu.nl", "infob3it/XXX/", "Rose", "studentXXX", ""}, moistureThreshold {waterThreshold} {}

inline const char* MQTT::getTopic(String topicName, char* resultTopic) {
  (config.topicPrefix + topicName).toCharArray(resultTopic, MQTT_MAX_TOPIC_LENGTH);
  return resultTopic;
}

inline const char* MQTT::getPlantTopic(const __FlashStringHelper* topicName, char* resultTopic) {
  return getTopic(F("plant/") + String(config.plantName) + topicName, resultTopic);
}

void MQTT::init() {
  String(ESP.getChipId()).toCharArray(clientId, MQTT_MAX_CLIENT_ID_LENGTH);
  client = new MQTT_Client(config.server, PORT, clientId, config.username, config.password);
  client->will(getTopic(F("device/") + String(clientId) + F("/status"), statusTopic), "offline", 1, 1);

  getPlantTopic(F("/temp"), temperatureTopic);
  getPlantTopic(F("/press"), pressureTopic);
  getPlantTopic(F("/light"), lightTopic);
  getPlantTopic(F("/moist"), moistureTopic);

  waterSubscription = new Adafruit_MQTT_Subscribe(client, getPlantTopic(F("/water"), waterTopic), 1);
  senseSubscription = new Adafruit_MQTT_Subscribe(client, getPlantTopic(F("/sense"), senseTopic), 1);
  targetSubscription = new Adafruit_MQTT_Subscribe(client, getPlantTopic(F("/target"), targetTopic), 1);
  client->subscribe(waterSubscription);
  client->subscribe(senseSubscription);
  client->subscribe(targetSubscription);

  lastConnect = millis();
}

MQTTConfig& MQTT::getConfig() {
  return config;
}

void MQTT::connect() {
  lastConnect = millis();
  int8_t result = client->connect();
  if (result == 0) { // Success
    client->publish(statusTopic, "online", 1, true);
    Serial.println(F("Connected to MQTT Server!"));
    return;
  }

  Serial.print(F("MQTT connection to "));
  Serial.print(client->getConnectionString());
  Serial.print(": ");
  Serial.println(client->connectErrorString(result));

  if (client->connected())
    client->disconnect();
}

void MQTT::update() {
  if (client == NULL)
    return;
  
  if (!client->connected() && millis() - lastConnect >= CONNECTION_RETRY_TIMEOUT)
    connect();

  if (client->connected()) {
    while (Adafruit_MQTT_Subscribe *sub = client->readSubscription(5)) {
      if (sub == waterSubscription) {
        uint16_t value;
        if (sub->datalen == 2)
          value = (sub->lastread[0] << 8) | sub->lastread[1];
        else
          value = strtoul(reinterpret_cast<char*>(sub->lastread), NULL, 10);
        Serial.print("Watering: ");
        Serial.println(value);
        if (waterCallback != NULL)
          waterCallback(value);
      } else if (sub == senseSubscription) {
        char* sensor = reinterpret_cast<char*>(sub->lastread);
        Serial.print("Sensing ");
        Serial.println(sensor);
        if (senseCallback != NULL)
          senseCallback(sensor);
      } else if (sub == targetSubscription) {
        moistureThreshold = strtoul(reinterpret_cast<char*>(sub->lastread), NULL, 10);
        Serial.print("Set target moisture value to ");
        Serial.println(moistureThreshold);
      } else {
        Serial.println(F("Unknown subscription!"));
        break;
      }
    }
  }
}

void MQTT::setWaterCallback(void (*waterCallback)(uint16_t amount)) {
  this->waterCallback = waterCallback;
}

void MQTT::setSenseCallback(void (*senseCallback)(char* sensor)) {
  this->senseCallback = senseCallback;
}

void MQTT::publish(const char* topic, const char* value, bool manual) {
  char realTopic[MQTT_MAX_TOPIC_LENGTH + 2];
  strlcpy(realTopic, topic, MQTT_MAX_TOPIC_LENGTH);
  strcat_P(realTopic, manual ? PSTR("/m") : PSTR("/a"));
  client->publish(realTopic, value, manual ? 1U : 0U);
}

void MQTT::publish(const char* topic, float value, bool manual) {
  if (client != NULL && client->connected()) {
    char valueString[12]; // -9999999.99 is the mininum max length string, 99999999.99 is the maximum max length string
    if (value >= 100'000'000)
      strcpy_P(valueString, PSTR("MAX"));
    else if (value <= -10'000'000)
      strcpy_P(valueString, PSTR("MIN"));
    else
      dtostrf(value, 0, 2, valueString);
    publish(topic, valueString, manual);
  }
}

void MQTT::publish(const char* topic, int32_t value, bool manual) {
  if (client != NULL && client->connected()) {
    char valueString[12];
    itoa(value, valueString, 10);
    publish(topic, valueString, manual);
  }
}

void MQTT::publishTemperature(float value, bool manual) {
  publish(temperatureTopic, value, manual);
}

void MQTT::publishPressure(float value, bool manual) {
  publish(pressureTopic, value, manual);
}

void MQTT::publishLight(int value, bool manual) {
  publish(lightTopic, value, manual);
}

void MQTT::publishMoisture(int value, bool manual) {
  publish(moistureTopic, value, manual);
}

// Override the Adafruit_MQTT_Client to make it non-blocking
MQTT_Client::MQTT_Client(const char *server, uint16_t port,
                       const char *cid, const char *user, const char *pass):
                       Adafruit_MQTT_Client{&client, server, port, cid, user, pass} {}

String MQTT_Client::getConnectionString() {
  return String(username) + ":" + password + "@" + servername + "/" + will_topic;
}

uint16_t MQTT_Client::readPacket(uint8_t *buffer, uint16_t maxlen, int16_t timeout) {
  if (timeout > -1)
    return Adafruit_MQTT_Client::readPacket(buffer, maxlen, timeout);

  uint16_t len = 0;
  if (maxlen == 0) { // handle zero-length packets
    return 0;
  }

  if (client.connected()) {
    while (client.available()) {
      char c = client.read();
      buffer[len] = c;
      len++;

      if (len == maxlen) { // we read all we want, bail
        DEBUG_PRINT(F("Read data:\t"));
        DEBUG_PRINTBUFFER(buffer, len);
        return len;
      }
    }
  }
  return len;
}