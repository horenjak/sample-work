#ifndef Chart_H_
#define Chart_H_

#include "SSD1306Wire.h"
#include "OLEDDisplayUi.h"

#define SINGLE_PLOT_MODE 0
#define DOUBLE_PLOT_MODE 1
#define POINT_GEOMETRY_NONE 2
#define POINT_GEOMETRY_CIRCLE 3
#define POINT_GEOMETRY_SQUARE 4
#define POINT_GEOMETRY_TRIANGLE 5

// Copied and adapted from https://github.com/elC0mpa/OLED_SSD1306_Chart/
class OLED_Chart
{
private:
    OLEDDisplay *display;
    double _previous_x_coordinate, _previous_y_coordinate; //Previous point coordinates
    double _x_lower_left_coordinate, _y_lower_left_coordinate;   //Chart lower left coordinates
    double _chart_width, _chart_height;                          //Chart width and height
    double _y_min_values, _y_max_values;                   //Y axis Min and max values
    double _x_inc;                                               //X coordinate increment between values
    double _actual_x_coordinate;                                 //Actual point x coordinate
    double _xinc_div, _yinc_div;                                 //X and Y axis distance between division
    double _dig;
    char _point_geometry;  //Point geometry
    bool _y_labels_visible;   //Determines if the y labels should be shown
    String _y_min_label;    //Labels of the lower y value
    String _y_max_label;    //Labels of the higher y value
    double _x_drawing_offset; //Used to draw the char after the labels are applied
    bool _mid_line_visible;   //Determines if the mid line should be shown in Double plot mode

public:
    //Ctors
    OLED_Chart(): _y_min_label {""}, _y_max_label {""}
    {
        _point_geometry = POINT_GEOMETRY_NONE;
        _y_labels_visible = false;
        _mid_line_visible = true;
    }
    void init(OLEDDisplay *display);

    void setPlotMode(char mode);
    void setChartCoordinates(double x, double y);
    void setChartWidthAndHeight(double w, double h);
    void setYLimits(double ylo, double yhi);
    void setYLimitLabels(char *loLabel, char *hiLabel);
    void setYLabelsVisible(bool yLabelsVisible);
    void setPointGeometry(char pointGeometry);
    void setXIncrement(double xinc);
    void setAxisDivisionsInc(double xinc, double yinc);
    void setMidLineVisible(bool lineVisible);

    void drawChart();
    bool updateChart(double firstValue);
};

#endif