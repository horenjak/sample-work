#ifndef Display_H_
#define Display_H_

#include "screen.h"

class Display
{
private:
  SSD1306Wire display;
  Screen* screen;
  Screen* const* defaultScreens;
  uint8_t screenIndex;
  const uint8_t defaultScreenCount;
public:
  Display(uint8_t sdaPin, uint8_t sclPin,
    Screen** startScreens, uint8_t screenCount);
  void init();
  void update();
  void setScreen(Screen& screen);
  void nextScreen();
  OLEDDisplay& getDisplay();
};

#endif