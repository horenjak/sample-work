#ifndef TempPressure_H
#define TempPressure_H

#include <Wire.h>
#include <Adafruit_BMP280.h>


class TempPressure
{
private:
  bool connected;
  Adafruit_BMP280 bmp280;
public:
  TempPressure();
  void init();
  float getTemperature();
  float getPressure();
  void printTemperature();
  void printPressure();
};

#endif