#ifndef LightMoisture_H
#define LightMoisture_H

#include "../util.h"

#include <Arduino.h>

class LightMoisture
{
private:
  friend bool updateTimer(int self);
  friend bool enableSensor(int self);
  friend bool senseLight(int self);

  const unsigned long senseMoistureInterval = 30*60*1000, moistureStabilizeTime = 150, moistureReadTime = 50, sensorReadInterval = 3;
  const uint8_t pinInput;
  const uint8_t pinOutput;
  Average lightAverage, moistureAverage;
  bool sensorEnabled;
  bool moistureSensor; // by default light sensor on because moisture sensor turns on only when we need to read
  TIMER& timer;
  
  void read();
public:
  LightMoisture(uint8_t inputPin, uint8_t outputPin, TIMER& timer);
  bool tryGetCurrentLightValueAndReset(int& value);
  bool tryGetCurrentMoistureValueAndReset(int& value);
  void senseMoisture();
  void printLightSensor();
  void printMoistureSensor();
};

#endif