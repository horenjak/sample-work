#ifndef Util_H_
#define Util_H_

#include <Arduino.h>
#include <arduino-timer.h>

#define SIGN(x) ((0 < (x)) - ((x) < 0))

#define TIMER Timer<10, millis, int>

class Average {
private:
  float total;
  uint16_t count;
public:
  bool hasValue();
  void addValue(float value);
  float getAverage();
  void reset();
};

#endif