#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


//#define DEBUG

typedef struct Goods {
    char name [100];
    int sold;
}Goods;

typedef struct Sales{
    Goods * arrayOfSales;
    size_t arraySize;
    size_t numberOfGoods;
    size_t top_sold_count;
}Sales;
/// test presence of whitespace at the end of each command until the end of line
/// \param returnValue contains default return value
/// \return returns 0 if line contains something else then whitespace, 4 if EOF and returnValue if everything is correct
int testWhitespace(int returnValue)
{
    char whitespace;
    do{
        whitespace = getchar();
        if(isspace(whitespace) == 0){
            return 0;
        }
        if (feof ( stdin )){
            return 4;
        }
    }while(whitespace != '\n');
    return returnValue;
}
/// load all commands
/// \param goodsName used to return name of Goods for command "+"
/// \return returns 0 if input is incorrect, 1 if input is command "+", 2 if input is command "#", 3 if input is command "?" and 4 if input is EOF
int loadCommend(char *goodsName)
{
    char command;
    do{
        command = getchar();
        if (feof ( stdin )){
            return 4;
        }
    }while (isspace(command) != 0);
    switch(command){
        case '+':{
            if(scanf("%99s", goodsName) != 1){
                return 0;
            }
            return testWhitespace(1);
        }break;
        case '#':{
            return testWhitespace(2);
        }break;
        case '?':{
            return testWhitespace(3);
        }break;
        default:{
            return 0;
        }break;
    }
    return 4;
}
/// compare function  fo qsort
/// \param a first compared value
/// \param b second compared value
/// \return returns 1 if a < b, -1 if a > b and 0 if a == b
int compareGoods(const void * a, const void * b)
{
    Goods arg1 = *(const Goods*)a;
    Goods arg2 = *(const Goods*)b;

    if(arg1.sold < arg2.sold){
        return 1;
    }
    if(arg1.sold > arg2.sold){
        return -1;
    }
    if(arg1.sold == arg2.sold){
        if(strncmp(arg1.name, arg2.name, 100) > 0){
            return 1;
        }
        if (strncmp(arg1.name, arg2.name, 100) < 0){
            return -1;
        }
    }
    return 0;
}
/// finds goods based on name
/// \param allSales contains array of all goods and array info
/// \param goodsName contains name of product that we are looking for
/// \return returns index of found product or -1 if product is not in array
int findGoods(Sales allSales, char * goodsName)
{
    for(size_t i = 0; i < allSales.numberOfGoods; i++){
        if(strncmp(goodsName, allSales.arrayOfSales[i].name, 100) == 0){
            return i;
        }
    }
    return -1;
}
/// handles "+" command
/// \param allSales contains array of all goods and array info
/// \param newGoods name of new product
/// \return returns 1 if everything is correct and -1 if error occurred
int addSale(Sales * allSales, char * newGoods)
{
    int goodsIndex = findGoods(*allSales, newGoods);
    if(goodsIndex >= 0){
        allSales->arrayOfSales[goodsIndex].sold ++;
    }else{
        if(allSales->numberOfGoods >= allSales->arraySize){
            allSales->arraySize = allSales->arraySize * 2;
            Goods * tmp = (Goods*) realloc (allSales->arrayOfSales, allSales->arraySize * sizeof(Goods));
            if(tmp == NULL){
                free(allSales->arrayOfSales);
                return 0;
            }
            allSales->arrayOfSales = tmp;
        }
        strncpy(allSales->arrayOfSales[allSales->numberOfGoods].name, newGoods, 99);
        allSales->arrayOfSales[allSales->numberOfGoods].sold = 1;
        allSales->numberOfGoods++;
    }
    return 1;
}
/// count how many product in the array have the same number of sales
/// \param arrayOfGoods
/// \param arraySize
/// \param startIndex contains index of the first product with teh same number of sales
/// \return returns index of the last product with the same number of sales
int countSameSalesAmount(Goods * arrayOfGoods, size_t arraySize, size_t startIndex){
    size_t currentIndex = startIndex + 1;
    while(currentIndex < arraySize && arrayOfGoods[startIndex].sold == arrayOfGoods[currentIndex].sold){
        currentIndex ++;
    }
    return --currentIndex;
}
/// handles "#" commend
/// \param allSales contains information about all Goods
/// \param numberOfTopGoods number of the top goods to be listed
void listTopSales(Sales allSales, size_t numberOfTopGoods)
{
    allSales.top_sold_count = 0;
    qsort(allSales.arrayOfSales, allSales.numberOfGoods, sizeof(Goods), compareGoods);
    for(size_t i = 0; i < numberOfTopGoods && i < allSales.numberOfGoods; i++){
        size_t endIndex = countSameSalesAmount(allSales.arrayOfSales, allSales.numberOfGoods, i);
        if(endIndex > i){
            size_t startIndex = i;
            do{
                allSales.top_sold_count += allSales.arrayOfSales[i].sold;
                printf("%ld.-%ld. %s, %dx\n",startIndex + 1, endIndex + 1, allSales.arrayOfSales[i].name, allSales.arrayOfSales[i].sold);
                i++;
            }
            while (i <= endIndex);
            i--;
        }else{
            allSales.top_sold_count += allSales.arrayOfSales[i].sold;
            printf("%ld. %s, %dx\n",i + 1, allSales.arrayOfSales[i].name, allSales.arrayOfSales[i].sold);
        }
    }
    printf("Nejprodavanejsi zbozi: prodano %ld kusu\n", allSales.top_sold_count);
}
/// handles "?" commend
/// \param allSales contains information about all Goods
/// \param numberOfTopGoods number of the top goods to be listed
void topSalesCount(Sales allSales, size_t numberOfTopGoods)
{
    allSales.top_sold_count = 0;
    qsort(allSales.arrayOfSales, allSales.numberOfGoods, sizeof(Goods), compareGoods);
    for(size_t i = 0,j = 0; i < numberOfTopGoods && i < allSales.numberOfGoods && j < allSales.numberOfGoods ; i++,j++){
#ifdef DEBUG
        printf("value %d\n",  allSales.arrayOfSales[j].sold);
#endif
        allSales.top_sold_count += allSales.arrayOfSales[j].sold;
        if(j < allSales.numberOfGoods - 1 && i == numberOfTopGoods - 1 && allSales.arrayOfSales[numberOfTopGoods - 1].sold == allSales.arrayOfSales[j + 1].sold){
            i--;
        }
    }
    printf("Nejprodavanejsi zbozi: prodano %ld kusu\n", allSales.top_sold_count);
}

void printAllSales(Sales allSales)
{
    for(size_t i = 0; i < allSales.numberOfGoods; i++){
        printf("name = %s, amount = %d\n", allSales.arrayOfSales[i].name, allSales.arrayOfSales[i].sold);
    }
}

int main(){
    int numberOfTopGoods;
    Sales allSales; allSales.arraySize = 1000; allSales.numberOfGoods = 0; allSales.top_sold_count = 0;
    allSales.arrayOfSales = (Goods*) malloc (allSales.arraySize * sizeof(Goods));
    char goodsName[100];
    printf("Pocet sledovanych:\n");
    if(scanf("%d\n", &numberOfTopGoods) != 1 || numberOfTopGoods <= 0){
        if (feof ( stdin )){
            return 1;
        }
        printf("Nespravny vstup.\n");
        free(allSales.arrayOfSales);
        return 0;
    }
    printf("Pozadavky:\n");
    while(1){
        switch (loadCommend(goodsName))
        {
            case 0:{
                printf("Nespravny vstup.\n");
                free(allSales.arrayOfSales);
                return 0;
            }
                break;
            case 1:{
                if(!addSale(&allSales, goodsName)){
                    printf("Nespravny vstup.\n");
                    free(allSales.arrayOfSales);
                    return 0;
                }
            }
                break;
            case 2:{
                listTopSales(allSales, numberOfTopGoods);
            }
                break;
            case 3:{
                topSalesCount(allSales, numberOfTopGoods);
            }
                break;
            case 4:{
#ifdef DEBUG
                printf("end\n");
                printAllSales(allSales);
#endif
                free(allSales.arrayOfSales);
                return 1;
            }
        }
    }
}