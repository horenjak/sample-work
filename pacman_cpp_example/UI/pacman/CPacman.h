#ifndef PACMAN_02_CPACMAN_H
#define PACMAN_02_CPACMAN_H

#include "../tiles/CMovingTile.h"
#include "../ghosts/CGhost.h"
#include "../level/CLevel.h"

#include <vector>
#include <ncurses.h>

enum CherryLocation{
    CHERRYX = 16,
    CHERRYY = 15
};

class CPacman: public CMovingTile{
public:
    CPacman(std::set<CTile> roads, int x = 16, int y = 17, char content = PACMAN, int speed = 1,int direction = DOWN,
            int mode = STANDARD, size_t lives = 3,  int previousDirection = DOWN, size_t score = 0, size_t killCherryScore = 0,
            size_t killsInARow = 1, size_t livesAdded = 0, CTile previousPosition = CTile(16,17, PACMAN),
            bool cherryActive = false, bool ghostsCanBeKilled = true);
    void pacmanDirection (char userInput);
    void movePacman (char userInput);
    void checkPacmanGhostCollision(std::vector<std::shared_ptr<CGhost>> & ghosts, size_t & counter);
    bool checkIfLevelIsFinished();
    void resetPacman();
    void nextLevel(const CLevel & level, int difficulty);
    bool pacmanDead() const;
    size_t getScore() const;

    std::ostream & exportClass(std::ostream & os, int numberOfTabs) const override;
    virtual std::istream & importClass(std::istream & is) override;

private:
    size_t m_lives;
    int m_previousDirection;
    std::set<CTile> m_visitedRoads;
    std::set<CTile> m_usedPowerUps;
    size_t m_score;
    size_t m_killCherryScore;
    size_t m_killsInARow;
    size_t m_livesAdded;
    bool m_cherryActive;
    bool m_ghostsCanBeKilled;

    std::istream & importVisitedRoads(std::istream & is, char & separator);
    void score();
    void drawVisitedRoads();
    void checkPowerUps();
    void drawCherry();
    void pickUpCherry();

};

#endif //PACMAN_02_CPACMAN_H
