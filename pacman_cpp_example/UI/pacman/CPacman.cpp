#include "CPacman.h"

CPacman::CPacman(std::set<CTile> roads, int x, int y, char content, int speed,int direction, int mode,
                 size_t lives, int previousDirection, size_t score, size_t killCherryScore, size_t killsInARow,
                 size_t livesAdded, CTile previousPosition, bool cherryActive, bool ghostsCanBeKilled)
:CMovingTile(x, y, content, speed, direction, mode, previousPosition, std::move(roads)),
m_lives(lives),
 m_previousDirection(previousDirection),
 m_score(score),
 m_killCherryScore(killCherryScore),
 m_killsInARow(killsInARow),
 m_livesAdded(livesAdded),
 m_cherryActive(cherryActive),
 m_ghostsCanBeKilled(ghostsCanBeKilled)
{
    m_className = "CPacman";
}
/// based on user input saves in m_direction in which direction pacman suppose to move
/// \param userInput
void CPacman::pacmanDirection(char userInput) {
    if(userInput == 'w' || userInput == 'W'){
        m_direction = UP;
    }else if (userInput == 's' || userInput == 'S'){
        m_direction = DOWN;
    }else if (userInput == 'a' || userInput == 'A'){
        m_direction = LEFT;
    } else if (userInput == 'd' || userInput == 'D'){
        m_direction = RIGHT;
    }
}

void CPacman::movePacman(char userInput) {
    pacmanDirection(userInput);
    int x = m_x;
    int y = m_y;
    bool prevDirUsed = false;

    this->moveATile(m_direction);
    if(m_x == x && m_y == y){
        this->moveATile(m_previousDirection);
        prevDirUsed = true;
    }
    if(prevDirUsed == false){
        m_previousDirection = m_direction;
    }
    m_visitedRoads.insert(CTile(m_x, m_y, BLANK));
    drawVisitedRoads();
    checkPowerUps();
    drawCherry();
    pickUpCherry();

    if(m_mode == STANDARD) {
        mvaddch(m_y, m_x, PACMAN);
    } else{
        mvaddch(m_y, m_x, POWEREDUPPACMAN);
        m_mode--;
    }
    score();
}
/// this method every tic checks if pacman and any hunter collided if yes then based on pacman mode difficulty decides what to do
/// \param ghosts contains information about all ghosts
/// \param counter send tic counter because if pacman dies ticCounter needs to be rested
void CPacman::checkPacmanGhostCollision(std::vector<std::shared_ptr<CGhost>> & ghosts, size_t & counter) {
    CTile currentPacmanPosition(m_x, m_y, m_content);
    for(auto & x: ghosts){
        CTile currentGhostPosition (x->getX(), x->getY(), x->getContent());
        if(currentPacmanPosition == currentGhostPosition ||
           m_previousPosition == x->getPreviousPosition() ||
           (currentPacmanPosition == x->getPreviousPosition() && currentGhostPosition == m_previousPosition))
        {
            if(m_mode == STANDARD) {
                m_lives--;
                this->resetPacman();
                for (auto &y : ghosts) {
                    y->resetGhost(ghosts);
                }
                counter = 0;
            }else{
                if(m_ghostsCanBeKilled) {
                    x->resetGhost(ghosts);
                    m_killCherryScore += 200 * m_killsInARow;
                    m_killsInARow++;
                }
            }
            mvaddch(m_y,m_x,PACMAN);
            return;
        }
    }
}

bool CPacman::checkIfLevelIsFinished() {
    if(m_roads.size() == m_visitedRoads.size()){
        return true;
    }
    return false;
}

void CPacman::resetPacman() {
    m_x = PACMANX;
    m_y = PACMANY;
}
/// resets all pecman information
/// \param level send information abut level (in NORMAL difficulty after level 5 pacman can not kill ghost any more)
/// \param difficulty send information about game difficulty
void CPacman::nextLevel(const CLevel & level, int difficulty) {
    m_x = PACMANX;
    m_y = PACMANY;
    m_visitedRoads.clear();
    m_usedPowerUps.clear();
    m_direction = DOWN;
    m_previousDirection = DOWN;
    m_killCherryScore = 0;
    m_killsInARow = 0;
    m_cherryActive = false;
    m_score = 0;
    m_livesAdded = 0;
    m_mode = STANDARD;
    m_previousPosition = CTile(m_x,m_y,m_content);
    if(level.getLevel() >= 5 && difficulty != INVINCIBLE){
        m_ghostsCanBeKilled = false;
    }
}

bool CPacman::pacmanDead() const{
    if(m_lives == 0){
        return true;
    }
    return false;
}

size_t CPacman::getScore() const {
    size_t score = 0;
    score += m_score;
    score += 1000 * m_livesAdded;
    return score;
}

std::ostream & CPacman::exportClass(std::ostream &os, int numberOfTabs) const {
    CMovingTile::exportClass(os, numberOfTabs);
    tab(os,numberOfTabs); os << "\"lives\": " << m_lives << " ," << '\n';
    tab(os,numberOfTabs); os << "\"score\": " << m_score << " ," << '\n';
    tab(os,numberOfTabs); os << "\"killCherryScore\": " << m_killCherryScore << " ," << '\n';
    tab(os,numberOfTabs); os << "\"killsInRow\": " << m_killsInARow << " ," << '\n';
    tab(os,numberOfTabs); os << "\"livesAdded\": " << m_livesAdded << " ," << '\n';
    tab(os,numberOfTabs); os << "\"cherryActive\": " << m_cherryActive << " ," << '\n';
    tab(os,numberOfTabs); os << "\"ghostCanBeKilled\": " << m_ghostsCanBeKilled << " ," << '\n';
    tab(os,numberOfTabs); os << "\"visitedRoads\": " << "{ ";
    for(const CTile & x: m_visitedRoads){
        if(x == *m_visitedRoads.rbegin()){
            os << x << ' ';
        } else{
            os << x << ", " ;
        }
    }
    os << '}' << " ," << '\n';
    return os;
}

std::istream & CPacman::importClass(std::istream &is) {
    std::string header;
    char separator = ' ';

    CMovingTile::importClass(is);
    is >> header;
    if(header == "\"lives\":"){
        is >> m_lives >> separator;
    }
    propertiesSeparator(is, separator);
    is >> header;
    if(header == "\"score\":"){
        is >> m_score >> separator;
    }
    propertiesSeparator(is, separator);
    is >> header;
    if(header == "\"killCherryScore\":"){
        is >> m_killCherryScore >> separator;
    }
    propertiesSeparator(is, separator);
    is >> header;
    if(header == "\"killsInRow\":"){
        is >> m_killsInARow >> separator;
    }
    propertiesSeparator(is, separator);
    is >> header;
    if(header == "\"livesAdded\":"){
        is >> m_livesAdded >> separator;
    }
    propertiesSeparator(is, separator);
    is >> header;
    if(header == "\"cherryActive\":"){
        is >> m_cherryActive >> separator;
    }
    propertiesSeparator(is, separator);
    is >> header;
    if(header == "\"ghostCanBeKilled\":"){
        is >> m_ghostsCanBeKilled >> separator;
    }
    propertiesSeparator(is, separator);
    is >> header;
    if(header == "\"visitedRoads\":"){
        importVisitedRoads(is, separator);
    }
    if(m_visitedRoads.empty() == true){
        is.clear();
        is >> separator;
    }
    if(separator != '}'){
        throw std::invalid_argument ("Wrong separator for " + m_className + " in visitedRoads should be \'}\'");
    }
    is >> separator;
    propertiesSeparator(is, separator);
    return is;
}

std::istream & CPacman::importVisitedRoads(std::istream &is, char & separator) {
    is >> separator;
    if(separator != '{'){
        throw std::invalid_argument ("Wrong separator for " + m_className + " in visitedRoads should be \'{\'");
    }
    m_visitedRoads.clear();
    int x,y;
    char c;
    while(is >> x >> c >> y >> separator){
        if(c != ':'){
            throw std::invalid_argument ("Wrong separator for " + m_className + " in previousPosition should be \':\'");
        }
        if(separator == '}'){
            m_visitedRoads.insert(CTile(x, y, m_content));
            break;
        }
        propertiesSeparator(is, separator);
        m_visitedRoads.insert(CTile(x, y, m_content));
    }
    return is;
}

void CPacman::drawVisitedRoads() {
    for(auto & x : m_visitedRoads){
        mvaddch(x.getY(), x.getX(), BLANK);
    }
}
///check if pacman picked up any power up
void CPacman::checkPowerUps() {
    for(const CTile & x : POWERUPSSET){
        if(x.getX() == m_x && x.getY() == m_y){
            CTile powerUp(m_x, m_y, POWERUP);
            auto it = m_usedPowerUps.find(powerUp);
            if(it == m_usedPowerUps.end()) {
                m_killsInARow = 1;
                m_mode = POWEREDUP;
                m_usedPowerUps.insert(powerUp);
            }
        }
    }
}
///calculate and prints on screen pacman score for current level
void CPacman::score() {
    m_score = (m_visitedRoads.size() - m_usedPowerUps.size()) + m_killCherryScore - (1000 * m_livesAdded);
    std::string score = std::to_string(m_score);
    mvprintw(2, 8, (score).c_str());

    if(m_score >= (1000 * (m_livesAdded + 1))){
        m_lives ++;
        m_livesAdded ++;
    }

    std::string lives = std::to_string(m_lives);
    mvprintw(22, 8, (lives).c_str());
}
///draws cherry on a board when pacman reaches certain score
void CPacman::drawCherry() {
    if(m_score - m_killCherryScore == 70 || m_score - m_killCherryScore == 200 ){
        m_cherryActive = true;
    }
    if(m_cherryActive){
        mvaddch(CHERRYY, CHERRYX, CHERRY);
    }
}
///checks if pacman picked up cherry
void CPacman::pickUpCherry() {
    if(m_x == CHERRYX && m_y == CHERRYY && m_cherryActive == true){
        m_killCherryScore += 200;
        m_cherryActive = false;
    }
}