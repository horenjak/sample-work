#ifndef PACMAN_02_CGHOSTCHICKEN_H
#define PACMAN_02_CGHOSTCHICKEN_H

#include "CGhost.h"

const static int GHOSTCHICKENY = 12;

class CGhostChicken : public CGhost{
public:
    explicit CGhostChicken(std::set<CTile> roads, int x = GHOSTX, int y = GHOSTCHICKENY, char content = GHOSTCHICKEN, int speed = 2,
                           int direction = UP, int mode = CHASE, int startingDelay = 60,
                           CTile previousPosition = CTile(16,12,GHOSTCHICKEN));
    virtual ~CGhostChicken() = default;
    void resetGhost(const std::vector<std::shared_ptr<CGhost>> &ghosts) override;

private:
    CTile computeDestinationPoint(const CMovingTile &pacman,const CTile & hunter) const override;
};

#endif //PACMAN_02_CGHOSTCHICKEN_H
