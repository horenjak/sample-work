#ifndef PACMAN_02_CGHOSTHUNTER_H
#define PACMAN_02_CGHOSTHUNTER_H

#include "CGhost.h"
#include "../pacman/CPacman.h"

#include <math.h>


class CGhostHunt : public CGhost{
public:
    explicit CGhostHunt(std::set<CTile> roads, int x = GHOSTX, int y = HUNTERY, char content = GHOSTHUNTER, int speed = 2, int direction = LEFT,
                        int mode = CHASE, bool missileMode = false, int startingDelay = 0, CTile previousPosition = CTile(16,9,GHOSTHUNTER));
    virtual ~CGhostHunt() = default;
    void resetGhost (const std::vector<std::shared_ptr<CGhost>> &ghosts) override;

    std::ostream & exportClass(std::ostream & os, int numberOfTabs) const override;
    std::istream & importClass(std::istream & is) override;

protected:
    void determineGhostMode(const CMovingTile & pacman, int & scatterClock, int pacmanScore) override;
    CTile computeDestinationPoint(const CMovingTile &pacman, const CTile & hunter) const override;

private:
    bool m_missileMode;

};

#endif //PACMAN_02_CGHOSTHUNTER_H
