#include "CGhostVector.h"

CGhostVector::CGhostVector(std::set<CTile> roads, int x, int y, char content, int speed, int direction, int mode,
                           int startingDelay, CTile previousPosition)
:CGhost(x,y,content,std::move(roads), previousPosition,speed,direction,mode, startingDelay)
{
    m_className = "CGhostVector";
}

void CGhostVector::resetGhost(const std::vector<std::shared_ptr<CGhost>> &ghosts) {
    CGhost::resetGhost(ghosts);
    m_y = GHOSTVECTORY;
    m_previousPosition = CTile(m_x, m_y, m_content);
    m_startingDelay = 40;
}

// in mode CHASE: to get Vectors destination point you have to take vector that starts on the position of the hunter
// and end two tiles in front of pacman and then double it and that is where Vectors destination point is
// in mode SCATTER: Vector runs to the bottom right corner his destination point is in the bottom right corner of the screen
CTile CGhostVector::computeDestinationPoint(const CMovingTile &pacman, const CTile &hunter) const {
    std::pair<int, int> vectorHunterVector;

    if(m_mode == CHASE) {
        vectorHunterVector = std::make_pair((pacman.getX() - hunter.getX()) * 2, (pacman.getY() - hunter.getY()) * 2);
        return CTile(vectorHunterVector.first + hunter.getX(), vectorHunterVector.second + hunter.getY(), GHOSTVECTOR);
    }else if(m_mode == FRIGHTENED){
        return pacman;
    } else{
        return CTile(WIDTH + 1, HEIGHT + 1, GHOSTVECTOR);
    }

}

