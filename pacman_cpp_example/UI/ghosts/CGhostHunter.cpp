#include "CGhostHunter.h"

CGhostHunt::CGhostHunt(std::set<CTile> roads, int x, int y, char content, int speed, int direction, int mode,
                       bool missileMode, int startingDelay, CTile previousPosition)
:CGhost(x, y, content, std::move(roads), previousPosition, speed, direction, mode, startingDelay),
m_missileMode(missileMode){
    m_className = "CGhostHunter";
}

void CGhostHunt::resetGhost(const std::vector<std::shared_ptr<CGhost>> &ghosts) {
    CGhost::resetGhost(ghosts);
    int i = 0;
    m_y = m_y - 1;
    ///on INSANE difficulty there are only ghosts Hunters and its needed to set them different starting points and starting delays
    for(auto & ghost: ghosts){
        if(*ghost == *this && ghost->getContent() == GHOSTHUNTER){
            break;
        }
        ++i;
    }
    m_y = HUNTERY + i;
    m_startingDelay = 10 * i;
    m_missileMode = false;
}

// in mode CHASE: hunter chase pacman its destination point is pacman's location
// in mode SCATTER: hunter go to top right corner its destination point is tile in top right corner of the screen
// in mode guidedMissile: hunter  behavior changes twice during a level he speeds up and in mode SCATTER just turns around but still chase the pacman
CTile CGhostHunt::computeDestinationPoint(const CMovingTile & pacman, const CTile & hunter) const {
    if(m_missileMode){
        return pacman;
    }else if(m_mode == SCATTER){
        return CTile(WIDTH + 1,1, GHOSTHUNTER);
    } else if (m_mode == CHASE || m_mode == FRIGHTENED){
        return pacman;
    }
    return pacman;
}

void CGhostHunt::determineGhostMode(const CMovingTile &pacman, int &scatterClock, int pacmanScore) {
    if(pacmanScore > 500){
        m_missileMode = true;
        turnAround();
    }
    if(pacman.getMode() > STANDARD){
        if(m_mode != FRIGHTENED){
            this->turnAround();
        }
        m_mode = FRIGHTENED;
    }else if(scatterClock != 0 && m_missileMode == false){
        m_mode = SCATTER;
        scatterClock --;
    }else{
        m_mode = CHASE;
    }
}

std::ostream & CGhostHunt::exportClass(std::ostream &os, int numberOfTabs) const {
    CGhost::exportClass(os,numberOfTabs);
    tab(os,numberOfTabs); os << "\"missileMode\": " << m_missileMode << " ," << '\n';
    return os;
}

std::istream & CGhostHunt::importClass(std::istream &is) {
    std::string header;
    char separator = ' ';

    CGhost::importClass(is);
    is >> header;
    if(header == "\"missileMode\":"){
        is >> m_missileMode >> separator;
    }
    propertiesSeparator(is, separator);
    return is;
}