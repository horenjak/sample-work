#include "CGhostChicken.h"

CGhostChicken::CGhostChicken(std::set<CTile> roads, int x, int y, char content, int speed, int direction, int mode,
                             int startingDelay, CTile previousPosition)
:CGhost(x,y,content, std::move(roads), previousPosition, speed,direction,mode, startingDelay){
    m_className = "CGhostChicken";
}

void CGhostChicken::resetGhost(const std::vector<std::shared_ptr<CGhost>> &ghosts) {
    CGhost::resetGhost(ghosts);
    m_y = GHOSTCHICKENY;
    m_previousPosition = CTile(m_x, m_y, m_content);
    m_startingDelay = 60;
}

// in mode CHASE: Chickens destination point is pacman's position if his distance from him is grater then 8
// if chicken get too close his destination point changes to bottom left corner of the screen
// in mode SCATTER: chicken runs to bottom left corner his destination point is bottom left corner of the screen
CTile CGhostChicken::computeDestinationPoint(const CMovingTile &pacman,const CTile & hunter) const {
    int distanceFromPacman = pointDistance((CTile)*this, (CTile)pacman);
    if((m_mode == CHASE && distanceFromPacman > 8) || m_mode == FRIGHTENED){
        return pacman;
    }else{
        return CTile(0,HEIGHT + 1, GHOSTCHICKEN);
    }
}
