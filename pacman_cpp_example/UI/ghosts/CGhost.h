#ifndef PACMAN_02_CGHOST_H
#define PACMAN_02_CGHOST_H

#include "../tiles/CMovingTile.h"

#include <math.h>
#include <exception>
#include <vector>
#include <iostream>
#include <optional>

enum GhostModes {
    CHASE = 1,
    SCATTER = 2,
    FRIGHTENED = 3
};

class CGhost : public CMovingTile{
public:
    CGhost(int x, int y , char content, std::set<CTile> roads, CTile previousPosition , int speed = 2,
           int direction = LEFT, int mode = CHASE, int startingDelay = 0);
    virtual void resetGhost(const std::vector<std::shared_ptr<CGhost>> &ghosts);
    virtual void moveGhost(const CMovingTile & pacman, int & scatterCounter, const CTile & hunter, int  pacmanScore = 0);
    int getGhostsStaringDelay() const;

    std::ostream & exportClass(std::ostream & os, int numberOfTabs) const override;
    std::istream & importClass(std::istream & is) override;

protected:
    int m_startingDelay;
    std::vector<std::pair<CTile,int>> m_crossRoadType;
    bool testCrossroads(std::vector<std::pair<CTile,int>> &possibleDirections);
    static double pointDistance(const CTile & pointA, const CTile & pointB);
    void turnAround();
    int longestDistanceDirection(const std::vector<std::pair<CTile,int>> & m_crossRoadType, CTile destinationPoint) const;
    int shortestDistanceDirection(const std::vector<std::pair<CTile,int>> & m_crossRoadType, CTile destinationPoint) const;
    virtual void determineGhostMode(const CMovingTile &pacman, int &scatterClock, int pacmanScore);
    virtual CTile computeDestinationPoint(const CMovingTile &pacman,const CTile & hunter) const;
    static double compareNumbers(double a, double b);
};

#endif //PACMAN_02_CGHOST_H
