#include "CGhostTrapper.h"

CGhostTrapper::CGhostTrapper(std::set<CTile> roads, int x, int y, char content, int speed, int direction, int mode,
                             int startingDelay, CTile previousPosition)
:CGhost(x,y,content,std::move(roads), previousPosition, speed,direction,mode, startingDelay){
    m_className = "CGhostTrapper";
}

void CGhostTrapper::resetGhost(const std::vector<std::shared_ptr<CGhost>> &ghosts) {
    CGhost::resetGhost(ghosts);
    m_y = GHOSTTRAPPERY;
    m_previousPosition = CTile(m_x, m_y, m_content);
    m_startingDelay = 20;
}

// in mode CHASE: trappers destination point is four tiles in front of pacman
// in mode SCATTER: trapper goes to top left corner his destination  point is in the top left corner of the screen
CTile CGhostTrapper::computeDestinationPoint(const CMovingTile &pacman, const CTile & hunter) const {
    if(m_mode == SCATTER){
        return CTile(0,0,GHOSTTRAPPER);
    }else if(m_mode == CHASE){
        int destinationX = pacman.getX();
        int destinationY = pacman.getY();
        switch(pacman.getDirection()) {
            case UP: {
                destinationY = destinationY - 4;
                break;
            }
            case DOWN:{
                destinationY = destinationY + 4;
                break;
            }
            case LEFT:{
                destinationX = destinationX - 4;
                break;
            }
            case RIGHT:{
                destinationX = destinationX + 4;
                break;
            }
        }
        return CTile(destinationX, destinationY, GHOSTTRAPPER);
    }
    return pacman;
}
