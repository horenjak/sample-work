#ifndef PACMAN_02_CGHOSTTRAPPER_H
#define PACMAN_02_CGHOSTTRAPPER_H

#include "CGhost.h"

const static int GHOSTTRAPPERY = 10;

class CGhostTrapper : public CGhost{
public:
    explicit CGhostTrapper(std::set<CTile> roads, int x = GHOSTX, int y = GHOSTTRAPPERY, char content = GHOSTTRAPPER, int speed = 2,
                           int direction = UP, int mode = CHASE, int startingDelay = 20, CTile previousPosition = CTile(16,10,GHOSTTRAPPER));
    virtual ~CGhostTrapper() = default;

    void resetGhost(const std::vector<std::shared_ptr<CGhost>> &ghosts) override;

private:
    CTile computeDestinationPoint(const CMovingTile &pacman, const CTile & hunter) const override;
};

#endif //PACMAN_02_CGHOSTTRAPPER_H
