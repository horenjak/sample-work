#ifndef PACMAN_02_CGHOSTVECTOR_H
#define PACMAN_02_CGHOSTVECTOR_H

#include "CGhost.h"


const static int GHOSTVECTORY = 11;

class CGhostVector : public CGhost {
public:
    explicit CGhostVector(std::set<CTile> roads, int x = GHOSTX, int y = GHOSTVECTORY, char content = GHOSTVECTOR,
                          int speed = 2, int direction = UP, int mode = CHASE, int startingDelay = 40,
                          CTile previousPosition = CTile(16,1, GHOSTVECTOR));
    virtual ~CGhostVector() = default;

    void resetGhost(const std::vector<std::shared_ptr<CGhost>> &ghosts) override;

private:
    CTile computeDestinationPoint(const CMovingTile &pacman, const  CTile &hunter) const override;
};

#endif //PACMAN_02_CGHOSTVECTOR_H
