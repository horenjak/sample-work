#include "CGhost.h"

CGhost::CGhost(int x, int y, char content,std::set<CTile> roads, CTile previousPosition, int speed, int direction, int mode, int startingDelay)
: CMovingTile(x, y, content, speed, direction, mode, previousPosition, std::move(roads)),
m_startingDelay(startingDelay){
    m_className = "CGhost";
}
/// look if ghost is currently located at crossroad
/// \param possibleDirections method saves all directions in which can ghost go on current crossroad
/// \return returns true if ghost is currently at a crossroad
bool CGhost::testCrossroads(std::vector<std::pair<CTile, int>> &possibleDirections) {
    possibleDirections.clear();
    std::set<std::pair<CTile,int>> directions = {std::make_pair(CTile(m_x,m_y + 1,m_content), DOWN),
                                                 std::make_pair(CTile(m_x,m_y - 1,m_content), UP),
                                                 std::make_pair(CTile(m_x + 1,m_y,m_content), RIGHT),
                                                 std::make_pair(CTile(m_x - 1,m_y,m_content),LEFT)};
    for(const auto & x: directions){
        auto it = m_roads.find(x.first);
        if(it != m_roads.end()){
          possibleDirections.push_back(x);
        }
    }
    if(possibleDirections.size() >= 3){
        return true;
    }
    if(possibleDirections.size() == 2){
        if(!(possibleDirections[0].second == DOWN && possibleDirections[1].second == UP) ||
           !(possibleDirections[0].second == RIGHT && possibleDirections[1].second == LEFT)){
            return true;
        }
    }
    return false;
}

void CGhost::resetGhost(const std::vector<std::shared_ptr<CGhost>> &ghosts) {
    m_x = GHOSTX;
    m_y = HUNTERY;
    m_mode = CHASE;
    m_direction = LEFT;
    m_startingDelay = 0;
    m_previousPosition = CTile(m_x,m_y,m_content);
}
///set ghosts mode and calls all methods needed to move ghost in proper direction
/// \param pacman contains pacman information
/// \param scatterClock contains countdown for ghost scattering
/// \param hunter contains information about ghost Hunter needed for computing ghost Vector destination point
/// \param pacmanScore contains player score for current level needed for turning on hunters missileMode
void CGhost::moveGhost(const CMovingTile &pacman, int &scatterClock, const CTile & hunter, int pacmanScore)
{
    determineGhostMode(pacman,scatterClock, pacmanScore);
    CTile destinationPoint = computeDestinationPoint(pacman, hunter);

    if(testCrossroads(m_crossRoadType)){
        if(m_crossRoadType.size() == 2){
            if(abs(m_direction - m_crossRoadType[0].second) == 1){
                m_direction = m_crossRoadType[1].second;
            } else{
                m_direction = m_crossRoadType[0].second;
            }
        } else {
            if(m_mode == CHASE || m_mode == SCATTER) {
                m_direction = shortestDistanceDirection(m_crossRoadType, destinationPoint);
            }else if(m_mode == FRIGHTENED){
                m_direction = longestDistanceDirection(m_crossRoadType, destinationPoint);
            }
        }
    }
    if(m_x == GHOSTX && (m_y >= 10 && m_y <= 14)){
        m_y = HUNTERY;
    }
    moveATile(m_direction);
}

int CGhost::getGhostsStaringDelay() const{
    return m_startingDelay;
}

double CGhost::compareNumbers(double a, double b){
    if(a > b){
        return a;
    }
    return b;
}

double CGhost::pointDistance(const CTile &pointA, const CTile &pointB) {
    double distance;
    if(pointA.getX() == TELEPORTX && pointA.getY() == TELEPORTTOPY){
        distance = compareNumbers(
                sqrt(pow(pointB.getX() - 16, 2) +
                     pow(pointB.getY() - HEIGHT, 2)) + 4,
                sqrt(pow(pointB.getX() - pointA.getX(), 2) +
                     pow(pointB.getY() - pointA.getY(), 2))
                );
    } else if(pointA.getX() == TELEPORTX && pointA.getY() == TELEPORTBOTY ){
        distance = compareNumbers(
                sqrt(pow(pointB.getX() - 16, 2) +
                     pow(pointB.getY() - 3, 2)) + 4,
                sqrt(pow(pointB.getX() - pointA.getX(), 2) +
                     pow(pointB.getY() - pointA.getY(), 2))
        );
    }else {
        distance = sqrt(pow(pointB.getX() - pointA.getX(), 2) +
                               pow(pointB.getY() - pointA.getY(), 2));
    }
    return distance;
}

void CGhost::turnAround() {
    if(m_direction  == UP){
        m_direction = DOWN;
    } else if(m_direction == DOWN){
        m_direction = UP;
    } else if(m_direction == LEFT){
        m_direction = RIGHT;
    }else{
        m_direction = LEFT;
    }
}
/// compute longest distance rout from destination point used in ghost FRIGHTENED mode
/// \param crossRoadType
/// \param destinationPoint
/// \return returns direction in which should ghost go
int CGhost::longestDistanceDirection(const std::vector<std::pair<CTile, int>> &crossRoadType, CTile destinationPoint) const {
    double longestDistance = 0;
    int direction = m_direction;
    for (const std::pair<CTile, int> &x : crossRoadType) {
        double testedPointDistance = pointDistance(x.first, destinationPoint);
        if (testedPointDistance > longestDistance) {
            longestDistance = testedPointDistance;
            direction = x.second;
        }
    }
    return direction;
}
/// compute shortest distance rout from destination point used in ghost CHASE mode
/// \param crossRoadType
/// \param destinationPoint
/// \return returns direction in which should ghost go
int CGhost::shortestDistanceDirection(const std::vector<std::pair<CTile, int>> &crossRoadType, CTile destinationPoint) const {
    double shortestDistance = 1000;
    int direction = m_direction;
    for (const std::pair<CTile, int> &x : crossRoadType) {
        double testedPointDistance = pointDistance(x.first, destinationPoint);
        if (testedPointDistance < shortestDistance) {
            shortestDistance = testedPointDistance;
            direction = x.second;
        }
    }
    return direction;
}
/// determine ghost mode basted on pacman mode scatter clock
/// \param pacman
/// \param scatterClock
/// \param pacmanScore is used only by ghost Hunter which goes to missile mode if pacman score is > 500
void CGhost::determineGhostMode(const CMovingTile &pacman, int &scatterClock, int pacmanScore) {
    if(pacman.getMode() > STANDARD){
        if(m_mode != FRIGHTENED){
            this->turnAround();
        }
        m_mode = FRIGHTENED;
    }else if(scatterClock != 0){
        m_mode = SCATTER;
        scatterClock --;
    }else{
        m_mode = CHASE;
    }
}

CTile CGhost::computeDestinationPoint(const CMovingTile &pacman, const CTile &hunter) const {
    return pacman;
}

std::ostream & CGhost::exportClass(std::ostream &os, int numberOfTabs) const {
    CMovingTile::exportClass(os, numberOfTabs);
    tab(os,numberOfTabs); os << "\"startingDelay\": " << m_startingDelay << " ," << '\n';
    return os;
}

std::istream & CGhost::importClass(std::istream &is) {
    std::string header;
    char separator = ' ';

    CMovingTile::importClass(is);
    is >> header;
    if(header == "\"startingDelay\":"){
        is >> m_startingDelay >> separator;
    }
    propertiesSeparator(is, separator);
    return is;
}