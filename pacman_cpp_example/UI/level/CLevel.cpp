#include "CLevel.h"

CLevel::CLevel(size_t level)
:m_level(level)
{
    loadLevel();
}

void drawLayout(size_t level){
    mvprintw(0,0, (HEADER).c_str());
    mvprintw(1,13, (LEVEL).c_str());
    std::string levelSTR = std::to_string(level);
    mvprintw(1,19, (levelSTR).c_str());
    mvprintw(2, 2, (SCORE).c_str());
    mvprintw(22, 2, (LIVES).c_str());
}

void CLevel::drawLevel() const {
    drawLayout(m_level);
    for(int i = 3; i < HEIGHT; ++i){
        for(int j = 0; j < WIDTH; ++j){
            mvaddch(i, j, WALL);
        }
    }
    for (const auto & x : m_roads){
        mvaddch(x.getY(), x.getX(), COIN);
    }
    for(const CTile & x : POWERUPSSET){
        mvaddch(x.getY(), x.getX(), POWERUP);
    }
}

std::set<CTile> CLevel::getRoads() const {
    return m_roads;
}

size_t CLevel::getLevel() const {
    return m_level;
}

CLevel & CLevel::operator++() {
    m_level++;
    return *this;
}

void CLevel::loadLevel() {
    std::ifstream ifs (LEVEL1, std::ios::in);

    if(!ifs.is_open()){
        throw std::runtime_error ("Fail to open!");
    }
    int x;
    int y;
    char c;
    while(ifs >> y >> c >> x){
        m_roads.insert(CTile(x,y,COIN));
    }
}

std::ostream & operator << (std::ostream & os, const CLevel & level){
    int numberOfTabs = 3;
    tab(os,numberOfTabs - 2); os << "\"CLevel\":[" << '\n';
    tab(os,numberOfTabs - 1); os << '{' << '\n';
    tab(os,numberOfTabs); os << "\"level\": " << level.m_level << " ," << '\n';
    tab(os,numberOfTabs - 1); os << '}' << '\n';
    tab(os,numberOfTabs - 2); os << ']' << '\n';
    return  os;
}

std::istream & propertiesSeparatorLevel (std::istream & is, char separator){
    if(separator != ','){
        throw std::invalid_argument ("Wrong separator for Player properties should be separated by \',\'");
    }
    return is;
}

std::istream & operator >> (std::istream & is, CLevel & level){

    std::string header;
    char separator;

    is >> header;
    if(header == "\"CLevel\":["){
        is >> separator;
        if(separator == '{'){
            is >> header;
            if(header == "\"level\":"){
                is >> level.m_level >> separator;
            }
            propertiesSeparatorLevel(is, separator);
        }else  {
            throw std::invalid_argument ("Wrong separator for Level should be \'{\'");
        }
        is >> separator;
        if(separator != '}'){
            throw std::invalid_argument ("Wrong separator for Level should be \'}\'");
        }
    } else{
        throw std::invalid_argument ("Wrong header for Level");
    }
    is >> separator;
    if(separator != ']'){
        throw std::invalid_argument ("Wrong separator for Level should be \']\'");
    }
    return is;
}