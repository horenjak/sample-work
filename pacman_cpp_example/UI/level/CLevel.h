#ifndef PACMAN_02_CLEVEL_H
#define PACMAN_02_CLEVEL_H

#include "../tiles/CTile.h"
#include "../../EnumsConsts.h"

#include <set>
#include <ncurses.h>
#include <iostream>
#include <fstream>

const static std::string SCORE = "score: ";
const static std::string LEVEL = "LEVEL: ";
const static std::string HEADER = "PACMAN";
const static std::string LIVES = "lives: ";
const static std::string LEVEL1 = "UI/level/level.txt";

class CLevel {
public:
    CLevel(size_t level = 1);
    void drawLevel () const;
    std::set<CTile> getRoads() const;
    size_t getLevel() const;
    CLevel& operator ++();

    friend std::ostream & operator << (std::ostream & os, const CLevel & level);
    friend std::istream & operator >> (std::istream & is, CLevel & level);

private:
    size_t m_level;
    std::set<CTile> m_roads;
    void loadLevel();
};

std::istream & propertiesSeparatorLevel (std::istream & is, char separator);

#endif //PACMAN_02_CLEVEL_H
