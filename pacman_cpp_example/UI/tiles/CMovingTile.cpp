#include "CMovingTile.h"

CMovingTile::CMovingTile(int x, int y, char content, int speed, int direction, int mode, CTile previousPosition, std::set<CTile> roads)
    : CTile(x, y, content),
    m_speed(speed),
    m_direction(direction),
    m_mode(mode),
    m_previousPosition(previousPosition),
    m_roads(std::move(roads))
{
    m_className = "CMovingTile";
}

int CMovingTile::getDirection() const {
    return m_direction;
}

int CMovingTile::getMode() const {
    return m_mode;
}

int CMovingTile::getSpeed() const {
    return m_speed;
}

CTile CMovingTile::getPreviousPosition() {
    return m_previousPosition;
}

struct moveATileOptions{
    bool m_teleport;
    int m_x_teleport_from;
    int m_y_teleport_from;
    int m_y_teleport_to;
    int m_x_direction;
    int m_y_direction;
};

/// moves object one tile
/// \param direction contains information in which direction object suppose to move
void CMovingTile::moveATile(int direction) {
    std::vector<moveATileOptions> directionsOptions(10);

    directionsOptions[UP] = {true,
                             16,
                             3,
                             21,
                             0,
                             -1};
    directionsOptions[DOWN] = {true,
                               16,
                               21,
                               3,
                               0,
                               1};
    directionsOptions[LEFT] = {false,
                               1000,
                               1000,
                               1000,
                               -1,
                               0};
    directionsOptions[RIGHT] = {false,
                               1000,
                               1000,
                               1000,
                               1,
                               0};
    if (directionsOptions[direction].m_teleport && m_x == directionsOptions[direction].m_x_teleport_from && m_y == directionsOptions[direction].m_y_teleport_from) {
        m_y = directionsOptions[direction].m_y_teleport_to;
    }
    auto it = m_roads.find(CTile(m_x + directionsOptions[direction].m_x_direction, m_y + directionsOptions[direction].m_y_direction, PACMAN));
    if(it != m_roads.end()){
        m_y = m_y + directionsOptions[direction].m_y_direction;
        m_x = m_x + directionsOptions[direction].m_x_direction;
    }
}

std::ostream & CMovingTile::exportClass(std::ostream &os, int numberOfTabs) const {
    tab(os,numberOfTabs - 2); os << "\"" << m_className << "\":[" << '\n';
    tab(os,numberOfTabs - 1); os << '{' << '\n';
    tab(os,numberOfTabs); os << "\"x\": " << m_x << " ," << '\n';
    tab(os,numberOfTabs); os << "\"y\": " << m_y << " ," << '\n';
    tab(os,numberOfTabs); os << "\"content\": " << m_content << " ," << '\n';
    tab(os,numberOfTabs); os << "\"speed\": " << m_speed << " ," << '\n';
    tab(os,numberOfTabs); os << "\"direction\": " << m_direction << " ," << '\n';
    tab(os,numberOfTabs); os << "\"mode\": " << m_mode << " ," << '\n';
    tab(os,numberOfTabs); os << "\"previousPosition\": " << m_previousPosition << " ," << '\n';
    return  os;
}

std::istream & CMovingTile::propertiesSeparator(std::istream &is, char separator) {
    if(separator != ','){
        throw std::invalid_argument ("Wrong separator for " + m_className + " properties should be separated by \',\'");
    }
    return is;
}

std::istream & CMovingTile::importClass(std::istream &is) {

    std::string header;
    char separator;

    is >> header;
    if(header == "\"" + m_className + "\":["){
        is >> separator;
        if(separator == '{'){
            is >> header;
            if(header == "\"x\":"){
                is >> m_x >> separator;
            }
            propertiesSeparator(is, separator);
            is >> header;
            if(header == "\"y\":"){
                is >> m_y >> separator;
            }
            propertiesSeparator(is, separator);
            is >> header;
            if(header == "\"content\":"){
                is >> m_content >> separator;
            }
            propertiesSeparator(is, separator);
            is >> header;
            if(header == "\"speed\":"){
                is >> m_speed >> separator;
            }
            propertiesSeparator(is, separator);
            is >> header;
            if(header == "\"direction\":"){
                is >> m_direction >> separator;
            }
            propertiesSeparator(is, separator);
            is >> header;
            if(header == "\"mode\":"){
                is >> m_mode >> separator;
            }
            propertiesSeparator(is, separator);
            is >> header;
            if(header == "\"previousPosition\":"){
                int x,y;
                char c;
                is >> x >> c >> y >> separator;
                if(c != ':'){
                    throw std::invalid_argument ("Wrong separator for " + m_className + " in previousPosition should be \':\'");
                }
                m_previousPosition = CTile(x,y,m_content);
            }
            propertiesSeparator(is, separator);
        }else  {
            throw std::invalid_argument ("Wrong separator for " + m_className + " should be \'{\'");
        }
    } else{
        throw std::invalid_argument ("Wrong header for " + m_className);
    }

    return is;
}

std::ostream & operator << (std::ostream & os, const CMovingTile & movingTile){
    int numberOfTabs = 3;
    movingTile.exportClass(os, numberOfTabs);
    tab(os,numberOfTabs - 1); os << '}' << '\n';
    tab(os,numberOfTabs - 2); os << ']' << '\n';
    return os;
}

std::istream & operator >> (std::istream & is, CMovingTile & movingTile){
    movingTile.importClass(is);
    char separator;
    is >> separator;
    if(separator != '}'){
        throw std::invalid_argument ("Wrong separator for " + movingTile.m_className + " should be \'}\'");
    }
    is >> separator;
    if(separator != ']'){
        throw std::invalid_argument ("Wrong separator for " + movingTile.m_className + " should be \']\'");
    }
    return is;
}