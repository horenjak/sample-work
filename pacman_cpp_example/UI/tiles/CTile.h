#ifndef PACMAN_02_CTILE_H
#define PACMAN_02_CTILE_H

#include <iostream>

const static int WIDTH = 31;
const static int HEIGHT = 22;

enum TilesBackgrounds{
    WALL = '*',
    COIN = '.',
    CHERRY = '$',
    POWERUP = '^',
    PACMAN = 'C',
    POWEREDUPPACMAN = 'c',
    GHOSTHUNTER = 'H',
    GHOSTTRAPPER = 'T',
    GHOSTVECTOR = 'V',
    GHOSTCHICKEN = 'K',
    BLANK = ' '
};

class CTile{
public:
    CTile(int x, int y, char content);
    int getX() const;
    int getY() const;
    char getContent() const;

    bool operator < (const CTile &a) const;
    bool operator == (const CTile &a) const;

    friend std::ostream & operator << (std::ostream & os, const CTile & tile);

protected:
    int m_x;
    int m_y;
    char m_content;
};

std::ostream  & tab (std::ostream & os, int numOfTabs);

#endif //PACMAN_02_CTILE_H
