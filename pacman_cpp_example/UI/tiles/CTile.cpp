#include "CTile.h"

CTile::CTile(int x, int y, char content)
:m_x(x), m_y(y), m_content(content) {
}

int CTile::getX() const {
    return m_x;
}

int CTile::getY() const {
    return m_y;
}

char CTile::getContent() const {
    return m_content;
}

bool CTile::operator<(const CTile &a) const{
    if(this->m_x < a.m_x){
        return true;
    }
    if(this->m_x == a.m_x && this->m_y < a.m_y){
        return true;
    }
    return false;
}

bool CTile::operator==(const CTile &a) const {
    if(m_x == a.getX() && m_y == a.getY()){
        return true;
    }
    return false;
}

std::ostream & operator << (std::ostream & os, const CTile & tile){
    return os << tile.m_x << ":" << tile.m_y;
}

/// this function is used for nice formatting of save files
std::ostream & tab(std::ostream &os, int numOfTabs){
    for(int i = 0; i < numOfTabs; ++i){
        os << '\t';
    }
    return os;
}