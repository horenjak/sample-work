#ifndef PACMAN_02_CMOVINGTILE_H
#define PACMAN_02_CMOVINGTILE_H

#include "../../EnumsConsts.h"
#include "CTile.h"
#include <set>
#include <vector>

enum PacmanModes{
    STANDARD = 1,
    POWEREDUP = 20
};

enum StartingPoints {
    PACMANX = 16,
    PACMANY = 17,
    GHOSTX = 16,
    HUNTERY = 9,
    TRAPPERX = 16,
    TRAPPERY = 8
};

enum Direction {
    LEFT = 1,
    RIGHT = 2,
    UP = 7,
    DOWN = 8,
};

enum TeleportCrossroads{
    TELEPORTX = 16,
    TELEPORTTOPY = 7,
    TELEPORTBOTY = 19
};

class CMovingTile : public CTile{
public:
    CMovingTile(int x, int y, char content, int speed, int direction, int mode, CTile previousPosition, std::set<CTile> roads);
    int getMode() const;
    int getSpeed() const;
    int getDirection() const;
    void moveATile(int direction);
    CTile getPreviousPosition();
    virtual std::ostream & exportClass(std::ostream & os, int numberOfTabs) const;
    virtual std::istream & importClass(std::istream & is);

    friend std::ostream & operator << (std::ostream & os, const CMovingTile & movingTile);
    friend std::istream & operator >> (std::istream & is, CMovingTile & movingTile);

protected:
    std::string m_className;
    int m_speed;
    int m_direction;
    int m_mode;
    CTile m_previousPosition;
    std::set<CTile> m_roads;
    std::istream & propertiesSeparator (std::istream & is, char separator);
};

#endif //PACMAN_02_CMOVINGTILE_H
