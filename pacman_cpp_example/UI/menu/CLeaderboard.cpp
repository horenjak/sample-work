#include "CLeaderboard.h"


CLeaderboard::CLeaderboard() {
    loadLeaderboard();
}

CLeaderboard::~CLeaderboard(){
    saveLeaderboard();
}
///loads current leaderboard from file
void CLeaderboard::loadLeaderboard() {
    m_leaderboard.clear();
    std::ifstream ifs ( LEADERBOARD, std::ios::in);

    if(!ifs.is_open()){
        throw std::runtime_error("File leaderboard could not be open");
    }
    if(ifs.peek() == std::ifstream::traits_type::eof()){
        return;
    }

    std::string header;
    std::string fileName;
    SLeaderboardRecord leaderboardRecord;
    char separator;

    ifs >> separator;
    if(separator != '{'){
        throw std::invalid_argument("Wrong separator at the start of leader board should be \'{\'");
    }
    while(ifs >> header >> fileName >> separator){
        if(header != "\"SLeaderboardRecord\":"){
            throw std::invalid_argument("Wrong header for SLeaderboardRecord");
        }
        if(separator != '['){
            throw std::invalid_argument("Wrong header separator for SLeaderboardRecord should be \'[\'");
        }
        ifs >> separator;
        if(separator != '{'){
            throw std::invalid_argument("Wrong header separator for SLeaderboardRecord should be \'{\'");
        }
        ifs >> header;
        if(header == "\"difficulty\":"){
            ifs >> leaderboardRecord.m_difficulty >> separator;
        }
        propertiesSeparator(separator);
        ifs >> header;
        if(header == "\"playerName\":"){
            ifs >> leaderboardRecord.m_playerName >> separator;
        }
        propertiesSeparator(separator);
        ifs >> header;
        if(header == "\"score\":"){
            ifs >> leaderboardRecord.m_score >> separator;
        }
        propertiesSeparator(separator);
        ifs >> separator;
        if(separator != '}'){
            throw std::invalid_argument("Wrong header separator for SLeaderboardRecord should be \'}\'");
        }
        ifs >> separator;
        if(separator != ']'){
            throw std::invalid_argument("Wrong header separator for SLeaderboardRecord should be \']\'");
        }
        m_leaderboard.push_back(std::make_pair(fileName,leaderboardRecord));
    }
    ifs.clear();
    ifs >> header; //todo figure out what is wrong with char here???
    //std::cout  << separator<< ifs.tellg() << std::endl;
    if(header != "}"){
        throw std::invalid_argument("Wrong separator at the end of leader board should be \'}\'");
    }
}
/// saves top ten players for Normal difficulty and top three for every other difficulty from current leaderboard to file
void CLeaderboard::saveLeaderboard() {
    std::sort(m_leaderboard.begin(), m_leaderboard.end(), comparePlayerScore);

    std::ofstream ostSaveLeaderboard(   LEADERBOARD, std::ios::out);
    ostSaveLeaderboard << '{' << std::endl;
    SRecordCountForDifficulties count;
    bool write = true;
    int numberOfTabs = 3;
    for(auto & leaderboardRecord: m_leaderboard){
        decideIfLeaderboardRecordWillBeSaved(leaderboardRecord.second.m_difficulty, count);
        if(write) {
            tab(ostSaveLeaderboard, numberOfTabs - 2);
            ostSaveLeaderboard << "\"SLeaderboardRecord\": " << leaderboardRecord.first << " [" << '\n';
            tab(ostSaveLeaderboard, numberOfTabs - 1);
            ostSaveLeaderboard << '{' << '\n';
            tab(ostSaveLeaderboard, numberOfTabs);
            ostSaveLeaderboard << "\"difficulty\": " << leaderboardRecord.second.m_difficulty << " ," << '\n';
            tab(ostSaveLeaderboard, numberOfTabs);
            ostSaveLeaderboard << "\"playerName\": " << leaderboardRecord.second.m_playerName << " ," << '\n';
            tab(ostSaveLeaderboard, numberOfTabs);
            ostSaveLeaderboard << "\"score\": " << leaderboardRecord.second.m_score << " ," << '\n';
            tab(ostSaveLeaderboard, numberOfTabs - 1);
            ostSaveLeaderboard << '}' << '\n';
            tab(ostSaveLeaderboard, numberOfTabs - 2);
            ostSaveLeaderboard << ']' << '\n';
        }
        write = true;
    }
    ostSaveLeaderboard << '}';
}

bool CLeaderboard::decideIfLeaderboardRecordWillBeSaved(int difficulty, SRecordCountForDifficulties & count) {
    switch (difficulty) {
        case NORMAL:{
            count.normal++;
            if(count.normal > 10){
                return false;
            }
            break;
        }
        case HARD:{
            count.hard++;
            if(count.hard > 3){
                return false;
            }
            break;
        }
        case INSANE:{
            count.insane++;
            if(count.insane > 3){
                return false;
            }
            break;
        }
        case INVINCIBLE:{
            count.invincible++;
            if(count.invincible > 3){
                return false;
            }
            break;
        }
    }
    return true;
}
///adds player status to current leaderboard
void CLeaderboard::addLeaderboardRecord(const CPlayer & player) {
    auto itFindUniqueFileName = std::find_if(m_leaderboard.begin(), m_leaderboard.end(),
                                            [&player](const std::pair<std::string, SLeaderboardRecord> & leaderboardRecord)
    {
        return leaderboardRecord.first == player.getPersonalFileName();
    });
    if(itFindUniqueFileName == m_leaderboard.end()){
        SLeaderboardRecord playersLeaderboardRecord;
        playersLeaderboardRecord.m_playerName = player.getPlayerName();
        playersLeaderboardRecord.m_difficulty = player.getDifficulty();
        playersLeaderboardRecord.m_score = player.getPlayerScore();
        m_leaderboard.push_back(std::make_pair(player.getPersonalFileName(),playersLeaderboardRecord));
    } else{
        if(itFindUniqueFileName->second.m_score < player.getPlayerScore()) {
            itFindUniqueFileName->second.m_score = player.getPlayerScore();
        }
    }
}

void CLeaderboard::displayLeaderboard() {
    std::sort(m_leaderboard.begin(), m_leaderboard.end(), comparePlayerScore);
    int normal = 0;
    int hard = 0;
    int insane = 0;
    int invincible = 0;

    mvprintw(LEADERBOARDNORMAL, LEADERBOARDX, "NORMAL:");
    mvprintw(LEADERBOARDHARD, LEADERBOARDX, "HARD:");
    mvprintw(LEADERBOARDINSANE, LEADERBOARDX, "INSANE:");
    mvprintw(LEADERBOARDINVINCIBLE, LEADERBOARDX, "INVINCIBLE:");

    for(int i = 0;i < m_leaderboard.size(); i++){
        switch (m_leaderboard[i].second.m_difficulty) {
            case NORMAL:{
                normal ++;
                if(normal <= 11){
                    mvprintw(LEADERBOARDNORMAL + normal, LEADERBOARDX, (m_leaderboard[i].second.m_playerName + ": " + std::to_string(m_leaderboard[i].second.m_score)).c_str());
                }
                break;
            }
            case HARD:{
                hard ++;
                if(hard <= 4){
                    mvprintw(LEADERBOARDHARD + hard, LEADERBOARDX, (m_leaderboard[i].second.m_playerName + ": " + std::to_string(m_leaderboard[i].second.m_score)).c_str());
                }
                break;
            }
            case INSANE:{
                insane ++;
                if(insane <= 4){
                    mvprintw(LEADERBOARDINSANE + insane, LEADERBOARDX, (m_leaderboard[i].second.m_playerName + ": " + std::to_string(m_leaderboard[i].second.m_score)).c_str());
                }
                break;
            }
            case INVINCIBLE:{
                invincible ++;
                if(invincible <= 4){
                    mvprintw(LEADERBOARDINVINCIBLE + invincible, LEADERBOARDX, (m_leaderboard[i].second.m_playerName + ": " + std::to_string(m_leaderboard[i].second.m_score)).c_str());
                }
                break;
            }
        }
    }
}

bool comparePlayerScore( std::pair<std::string ,SLeaderboardRecord> a , std::pair<std::string ,SLeaderboardRecord> b){
    if(a.second.m_score > b.second.m_score) {
        return true;
    }
    else if(a.second.m_score == b.second.m_score){
        if(a.second.m_playerName > b.second.m_playerName){
            return true;
        } else{
            return false;
        }
    }
    return false;
}

void propertiesSeparator (char separator){
    if(separator != ','){
        throw std::invalid_argument ("Wrong separator for Leaderboard properties should be separated by \',\'");
    }
}
