#include "CMenu.h"

CMenu::CMenu() {
    m_cursorY = 4;
    m_cursorX = 6;
    m_topCursorLimit = 4;
    m_botCursorLimit = BOTTOMCURSORLIMIT;
    m_selected = false;
}
/// displays game menu
/// \param player sends information abut player
/// \param pacman sends information about pacman
/// \param ghosts sends information about all ghosts
/// \param level sends information about level
/// \return returns true if exiting the game was selected else it send false
bool CMenu::displayMenu(CPlayer &player, CPacman &pacman, std::vector<std::shared_ptr<CGhost>> &ghosts,
                        CLevel &level)
{
    bool displayMenu = true;
    CMenu menu;
    CLoad load;
    CSave save;
    while(displayMenu){
        displayMenu = !menu.getKeyMenu();
        erase();
        menu.drawMenu();
        menu.printMenuCursor();
        switch(menu.selectionResult()){
            case NEWGAMEY:{
                menu.initialScreen(player,pacman,ghosts, level);
                return false;
            }
            case LOADGAMEY:{
                std::string fileName = menu.selectLodeFile();
                if(fileName == ""){
                    break;
                }
                load.loadSaveFile(fileName, ghosts, player, pacman, level);
                return false;
            }
            case SAVEGAMEY:{
                save.saveGame(pacman,ghosts,player, level);
                return false;
            }
            case RESTARTLEVEL:{
                load.loadNewGame(ghosts, pacman, player.getDifficulty(), level);
                return false;
            }
            case RESUMGAME:{
                displayMenu = false;
                break;
            }
            case EXITGAME:{
                return true;
            }
        }
        refresh();
    }
    return false;
}
/// displays initial screen where player enters his user name and selects difficulty or loads game from save file
/// \param player
/// \param pacman
/// \param ghosts
/// \param level
void CMenu::initialScreen(CPlayer &player, CPacman &pacman, std::vector<std::shared_ptr<CGhost>> &ghosts, CLevel & level)
{
    erase();
    CLeaderboard leaderboard;
    CLoad  load;
    std::string fileName;
    std::string name;
    char input;
    int difficulty = -1;

    while(difficulty < 0) {
        erase();
        leaderboard.displayLeaderboard();
        mvprintw(6,6,"Enter user name: ");
        mvprintw(7,6,"Or press ESC to load game");
        name = "";
        do {
            input = getch();
            if (isalpha(input) || isnumber(input)) {
                name += input;
                mvprintw(6, 23, name.c_str());
            } else if (input == BACKSPACE) {
                name.erase(name.length() - 1, name.length());
                erase();
                leaderboard.displayLeaderboard();
                mvprintw(6, 6, "Enter user name: ");
                mvprintw(7, 6, "Or press ESC to load game");
                mvprintw(6, 23, name.c_str());
            } else if (input == ESC) {
                fileName = selectLodeFile();
                if (fileName != "") {
                    load.loadSaveFile(fileName, ghosts, player, pacman, level);
                    return;
                }
                name = "";
                mvprintw(6, 6, "Enter user name: ");
                mvprintw(7, 6, "Or press ESC to load game");
            }
        } while (input != '\n' || name == "");
        player = CPlayer(name);
        difficulty = difficultySelectionMenu();
    }
    player.setDifficulty(difficulty);
    load.loadNewGame(ghosts, pacman, difficulty, level);
}

void CMenu::drawMenu() {
    CLeaderboard leaderboard;
    leaderboard.displayLeaderboard();
    mvprintw(NEWGAMEY, MENUSTAERTX, "new game");
    mvprintw(LOADGAMEY, MENUSTAERTX, "load game");
    mvprintw(SAVEGAMEY, MENUSTAERTX, "save game");
    mvprintw(RESTARTLEVEL, MENUSTAERTX, "restart level");
    mvprintw(RESUMGAME, MENUSTAERTX, "resume");
    mvprintw(EXITGAME, MENUSTAERTX, "exit");
}
/// process user input in menu
/// \return returns true if user press esc to close menu
bool CMenu::getKeyMenu() {
    char userInput = getch();
    if(userInput == ESC){
        return true;
    }else if((userInput == 'W' || userInput == 'w')){
        if(m_cursorY == m_topCursorLimit){
            m_cursorY = m_botCursorLimit;
        } else{
            m_cursorY --;
        }
    }else if((userInput == 'S' || userInput == 's')){
        if(m_cursorY == m_botCursorLimit){
            m_cursorY = m_topCursorLimit;
        } else{
            m_cursorY ++;
        }
    }else if(userInput == '\n'){
        m_selected = true;
    }
    return false;
}

void CMenu::printMenuCursor() const{
    mvprintw(m_cursorY, m_cursorX, "->");
}

int CMenu::selectionResult() const{
    if(m_selected == true){
        return m_cursorY;
    }
    return 0;
}
/// displays menu where player can select difficulty
/// \return returns selected difficulty
int CMenu::difficultySelectionMenu() {
    m_cursorY = NEWGAMEY;
    m_selected = false;
    m_botCursorLimit = 7;
    bool diffSelected = false;
    while(!diffSelected){
        diffSelected = getKeyMenu();
        erase();
        printMenuCursor();
        mvprintw(NORMALY, GAMEMODEMENUSTAERTX, "NORMAL");
        mvprintw(HARDY, GAMEMODEMENUSTAERTX, "HARD");
        mvprintw(INSANEY, GAMEMODEMENUSTAERTX, "INSANE");
        mvprintw(INVINCIBLEY, GAMEMODEMENUSTAERTX, "INVINCIBLE");
        switch (selectionResult()) {
            case NORMALY:{
                m_botCursorLimit = BOTTOMCURSORLIMIT;
                m_cursorY = GAMEDIFFICUTY;
                m_selected = false;
                return NORMAL;
            }
            case HARDY:{
                m_botCursorLimit = BOTTOMCURSORLIMIT;
                m_cursorY = GAMEDIFFICUTY;
                m_selected = false;
                return HARD;
            }
            case INSANEY:{
                m_botCursorLimit = BOTTOMCURSORLIMIT;
                m_cursorY = GAMEDIFFICUTY;
                m_selected = false;
                return INSANE;
            }
            case INVINCIBLEY:{
                m_botCursorLimit = BOTTOMCURSORLIMIT;
                m_cursorY = GAMEDIFFICUTY;
                m_selected = false;
                return INVINCIBLE;
            }
        }
    }
    m_cursorY = GAMEDIFFICUTY;
    m_botCursorLimit = BOTTOMCURSORLIMIT;
    return -1;
}
/// displays menu where player can select file that he wants to load
/// \return returns file name or blank "" if no file was selected
std::string CMenu::selectLodeFile() {
    erase();
    m_cursorY = NEWGAMEY;
    m_selected = false;

    std::ifstream ifsListOfSaveFiles (SAVEFILELIST, std::ios::in);

    std::string saveFileName = "";
    std::vector<std::string> saveFiles;
    int line = 0;

    while(ifsListOfSaveFiles >> saveFileName) {
        saveFiles.push_back(saveFileName);
        ++line;
    }

    m_botCursorLimit = line + 3;

    bool fileSelected = false;
    while(!fileSelected){
        line = 0;
        for(auto & saveFile : saveFiles){
            mvprintw(line + 4, 10, (saveFile).c_str());
            ++line;
        }
        fileSelected = getKeyMenu();
        erase();
        printMenuCursor();
        if(m_selected == true){
            m_botCursorLimit = BOTTOMCURSORLIMIT;
            std::cout << saveFiles[m_cursorY - 4];
            erase();
            return saveFiles[m_cursorY - 4];
        }
    }
    m_botCursorLimit = BOTTOMCURSORLIMIT;
    m_cursorY = LOADGAMEY;
    erase();
    return "";
}
/// at the end of each level displays players progress whit number of levels completed and overall score
/// \param player contains player information
/// \param level contains level information
/// \return  returns true if player presses Enter to continue to next level or false if he presses ESC to save and quit game
bool CMenu::displayProgress(const CPlayer &player, const CLevel &level)
{
    m_selected = false;
    erase();
    bool nextLevel = false;
    while(!nextLevel) {
        nextLevel = getKeyMenu();
        erase();
        mvprintw(PLAYERSCOREY, PROGRESSSUMMARYX, "SCORE: ");
        mvprintw(PLAYERSCOREY, PROGRESSSUMMARYX + 7, (std::to_string(player.getPlayerScore())).c_str());
        mvprintw(LEVELNUMBERY, PROGRESSSUMMARYX, "LEVEL: ");
        mvprintw(LEVELNUMBERY, PROGRESSSUMMARYX + 7, (std::to_string(level.getLevel() - 1)).c_str());
        mvprintw(LEVELNUMBERY, PROGRESSSUMMARYX + 7 + (std::to_string(level.getLevel() - 1)).length() , " COMPLETED" );
        mvprintw(NEXTLEVEL, PROGRESSSUMMARYX, "to continue to next level pres Enter");
        mvprintw(QUIT, PROGRESSSUMMARYX, "to save and quit pres Esc");
        if(m_selected){
            m_selected = false;
            return true;
        }
    }
    return false;
}
