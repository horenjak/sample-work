#ifndef PACMAN_02_CMENU_H
#define PACMAN_02_CMENU_H

#include "../../EnumsConsts.h"
#include "../../player/CPlayer.h"
#include "../../UI/ghosts/CGhost.h"
#include "../../UI/pacman/CPacman.h"
#include "../../UI/level/CLevel.h"
#include "../../save_lode/CLoad.h"
#include "../../save_lode/CSave.h"
#include "../../UI/menu/CLeaderboard.h"

#include "ncurses.h"
#include <fstream>
#include <vector>
#include <algorithm>
#include <iterator>

enum Menu{
    MENUSTAERTX = 10,
    NEWGAMEY = 4,
    LOADGAMEY = 5,
    SAVEGAMEY = 6,
    GAMEDIFFICUTY = 20,
    RESTARTLEVEL = 7,
    RESUMGAME = 8,
    EXITGAME = 9,
    BOTTOMCURSORLIMIT = 9
};

enum GameModesMenu{
    GAMEMODEMENUSTAERTX = 10,
    NORMALY = 4,
    HARDY = 5,
    INSANEY = 6,
    INVINCIBLEY = 7
};

enum ProgressSummary{
    PROGRESSSUMMARYX = 10,
    PLAYERSCOREY = 4,
    LEVELNUMBERY = 5,
    NEXTLEVEL = 7,
    QUIT = 8
};

class CMenu{
public:
    CMenu();

    bool displayMenu(CPlayer & player, CPacman & pacman, std::vector<std::shared_ptr<CGhost>> & ghosts, CLevel & level);
    void initialScreen(CPlayer & player, CPacman & pacman, std::vector<std::shared_ptr<CGhost>> &ghosts, CLevel & level);
    void drawMenu();
    bool getKeyMenu();
    void printMenuCursor() const;
    int selectionResult() const;
    int difficultySelectionMenu();
    std::string selectLodeFile();
    bool displayProgress(const CPlayer & player, const CLevel & level);

private:
    int m_cursorY;
    int m_cursorX;
    int m_botCursorLimit;
    int m_topCursorLimit;
    bool m_selected;

};

#endif //PACMAN_02_CMENU_H
