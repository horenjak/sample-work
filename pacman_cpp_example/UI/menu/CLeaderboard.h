#ifndef SRC_CLEADERBOARD_H
#define SRC_CLEADERBOARD_H

#include "../../UI/tiles/CTile.h"
#include "../../EnumsConsts.h"
#include "../../player/CPlayer.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

enum Leaderboard{
    LEADERBOARDX = 50,
    LEADERBOARDNORMAL = 4,
    LEADERBOARDHARD = 16,
    LEADERBOARDINSANE = 21,
    LEADERBOARDINVINCIBLE = 26
};

struct SRecordCountForDifficulties{
    int normal = 0;
    int hard = 0;
    int insane = 0;
    int invincible = 0;
};

struct SLeaderboardRecord{
    int m_difficulty;
    std::string m_playerName;
    size_t m_score;
};

class CLeaderboard{
public:
    CLeaderboard();
    ~CLeaderboard();
    void addLeaderboardRecord(const CPlayer & player);
    void displayLeaderboard();
    void loadLeaderboard();
    void saveLeaderboard();
private:
    bool decideIfLeaderboardRecordWillBeSaved(int difficulty, SRecordCountForDifficulties & count);
    std::vector<std::pair<std::string, SLeaderboardRecord>> m_leaderboard;
};

void propertiesSeparator (char separator);
bool comparePlayerScore(std::pair<std::string ,SLeaderboardRecord> a, std::pair<std::string ,SLeaderboardRecord> b);


#endif //SRC_CLEADERBOARD_H
