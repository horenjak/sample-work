#ifndef PACMAN_02_ENUMSCONSTS_H
#define PACMAN_02_ENUMSCONSTS_H

#include "UI/tiles/CTile.h"
#include <iostream>
#include <set>

const static char ESC = 27;
const static char BACKSPACE = 127;
const static std::string SAVEFILELIST = "save_lode/listOfSaveFiles";
const static std::string LEADERBOARD = "save_lode/leaderboard";

enum GameDifficulty{
    NORMAL = 1,
    HARD = 2, // ghost are faster and can not be killed
    INSANE = 3, // all ghosts are hunters and GhostModes dont change
    INVINCIBLE = 4 // ghosts are slower and can be always killed
};

const static std::set<CTile> POWERUPSSET = { CTile(7,5,POWERUP),
                                             CTile(29,5,POWERUP),
                                             CTile(7,19,POWERUP),
                                             CTile(29,19,POWERUP)};

#endif //PACMAN_02_ENUMSCONSTS_H
