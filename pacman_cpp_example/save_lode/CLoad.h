#ifndef PACMAN_02_CLOAD_H
#define PACMAN_02_CLOAD_H

#include "../UI/ghosts/CGhost.h"
#include "../UI/ghosts/CGhostHunter.h"
#include "../UI/ghosts/CGhostVector.h"
#include "../UI/ghosts/CGhostChicken.h"
#include "../UI/ghosts/CGhostTrapper.h"
#include "../player/CPlayer.h"
#include "../UI/pacman/CPacman.h"
#include "../UI/level/CLevel.h"

#include <string>
#include <vector>
#include <iostream>

class CLoad{
public:
    void loadSaveFile(std::string filename, std::vector<std::shared_ptr<CGhost>> & ghosts, CPlayer & player, CPacman & pacman, CLevel & level) const;
    void loadNewGame(std::vector<std::shared_ptr<CGhost>> & ghosts, CPacman & pacman, int difficulty, CLevel & level) const;

private:
    std::string selectFileBasedOnDifficulty(int difficulty) const;
    void makeGhostTemplates(std::vector<std::shared_ptr<CGhost>> & ghosts, const int difficulty) const;
};

#endif //PACMAN_02_CLOAD_H
