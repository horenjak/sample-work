#include "CSave.h"

const static std::string SAVEFILES = "save_lode/saveFiles/";

void CSave::saveGame(const CPacman &pacman, const std::vector<std::shared_ptr<CGhost>> &ghosts, const CPlayer &player, const CLevel & level) {
   std::ofstream ostSaveGame (   SAVEFILES + player.getPersonalFileName(), std::ios::out);

   if(!ostSaveGame.is_open()) {
       throw std::runtime_error("Could not open save file");
   }
   ostSaveGame << '{' << '\n';
   ostSaveGame << level;
   ostSaveGame << player;
   ostSaveGame << pacman;

   for (auto &ghost : ghosts) {
       ostSaveGame << *ghost;
   }
   ostSaveGame << '}' << '\n';

   ostSaveGame.close();

   loadAllFileNames();

   auto itFindFileRecord = m_allFileNames.find(player.getPersonalFileName());
   if(itFindFileRecord == m_allFileNames.end()) {

       std::ofstream ostSaveFileList(SAVEFILELIST, std::ios::app);

       if (!ostSaveFileList.is_open()) {
           throw std::runtime_error("Could not open save file list for write");
       }
       ostSaveFileList << player.getPersonalFileName() << '\n';
       ostSaveGame.close();
   }
   CLeaderboard leaderboard;
   leaderboard.addLeaderboardRecord(player);
}

void CSave::loadAllFileNames() {
    std::ifstream istSaveFileListCheck (SAVEFILELIST, std::ios::out);

    if(!istSaveFileListCheck.is_open()){
        throw std::runtime_error("Could not open save file list for read");
    }
    m_allFileNames.clear();
    std::string fileName;
    while(istSaveFileListCheck >> fileName){
        m_allFileNames.insert(fileName);
    }
    istSaveFileListCheck.close();
}

