#ifndef PACMAN_02_CSAVE_H
#define PACMAN_02_CSAVE_H

#include "../UI/pacman/CPacman.h"
#include "../player/CPlayer.h"
#include "../UI/ghosts/CGhost.h"
#include "../UI/level/CLevel.h"
#include "../UI/menu/CLeaderboard.h"

#include <iostream>
#include <ostream>
#include <set>
#include <vector>

class CSave {
public:
    void saveGame(const CPacman & pacman, const std::vector<std::shared_ptr<CGhost>> & ghosts, const CPlayer & player, const CLevel & level);
private:
    std::set<std::string> m_allFileNames;
    void loadAllFileNames();

};

#endif //PACMAN_02_CSAVE_H
