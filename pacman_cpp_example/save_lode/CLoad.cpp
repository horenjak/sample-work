#include "CLoad.h"

const static std::string SAVEFILES = "save_lode/saveFiles/";
const static std::string SETUPFILENORMADIFF = "save_lode/setUpFilesForDifficulties/NORMAL";
const static std::string SETUPFILEHARDDIFF = "save_lode/setUpFilesForDifficulties/HARD";
const static std::string SETUPFILEINSANEDIFF = "save_lode/setUpFilesForDifficulties/INSANE";
const static std::string SETUPFILEINVINCIBLEDIFF = "save_lode/setUpFilesForDifficulties/INVINCIBLE";

void CLoad::loadSaveFile(std::string filename, std::vector<std::shared_ptr<CGhost>> &ghosts, CPlayer &player, CPacman &pacman, CLevel & level) const
{
    std::ifstream ifs ( SAVEFILES + filename, std::ios::in);

    if(!ifs.is_open()){
        throw std::runtime_error("File could not be open");
    }
    char separator;
    ifs >> separator;
    if(separator != '{'){
        throw std::invalid_argument ("Wrong separator at the start of file should be \'{\'");
    }
    ifs >> level;
    ifs >> player;
    ifs >> pacman;

    makeGhostTemplates(ghosts,player.getDifficulty());

    for(auto & ghost : ghosts){
        ifs >> *ghost;
    }
    ifs >> separator;
    if(separator != '}'){
        throw std::invalid_argument ("Wrong separator at the end of file should be \'}\'");
    }

    ifs.close();
}

void CLoad::loadNewGame(std::vector<std::shared_ptr<CGhost>> &ghosts, CPacman &pacman, int difficulty, CLevel & level) const
{
    makeGhostTemplates(ghosts,difficulty);
    std::string fileName = selectFileBasedOnDifficulty(difficulty);

    std::ifstream ifs ( fileName, std::ios::in);

    if(!ifs.is_open()){
        throw std::runtime_error("File with set up difficulty could not be open");
    }
    char separator;
    ifs >> separator;
    if(separator != '{'){
        throw std::invalid_argument ("Wrong separator at the start of file should be \'{\'");
    }
    ifs >> level;
    ifs >> pacman;

    for(auto & ghost : ghosts){
        ifs >> *ghost;
    }

    ifs >> separator;
    if(separator != '}'){
        throw std::invalid_argument ("Wrong separator at the start of file should be \'}\'");
    }
    ifs.close();
}

std::string CLoad::selectFileBasedOnDifficulty(int difficulty) const {
    switch (difficulty) {
        case NORMAL:{
            return SETUPFILENORMADIFF;
        }
        case HARD:{
            return SETUPFILEHARDDIFF;
        }
        case INSANE:{
            return SETUPFILEINSANEDIFF;
        }
        case INVINCIBLE:{
            return SETUPFILEINVINCIBLEDIFF;
        }
        default:{
            throw std::runtime_error("Difficulty has not been selected");
        }
    }
}

void CLoad::makeGhostTemplates(std::vector<std::shared_ptr<CGhost>> &ghosts, const int difficulty) const {
    ghosts.clear();
    CLevel level;

    if(difficulty == INSANE){
        auto hunter = std::make_shared<CGhostHunt>(level.getRoads());
        auto hunter1 = std::make_shared<CGhostHunt>(level.getRoads());
        auto hunter2 = std::make_shared<CGhostHunt>(level.getRoads());
        auto hunter3 = std::make_shared<CGhostHunt>(level.getRoads());

        ghosts.push_back(hunter);
        ghosts.push_back(hunter1);
        ghosts.push_back(hunter2);
        ghosts.push_back(hunter3);
    }else{
        auto hunter = std::make_shared<CGhostHunt>(level.getRoads());
        auto trapper = std::make_shared<CGhostTrapper>(level.getRoads());
        auto vector = std::make_shared<CGhostVector>(level.getRoads());
        auto chicken = std::make_shared<CGhostChicken>(level.getRoads());

        ghosts.push_back(hunter);
        ghosts.push_back(trapper);
        ghosts.push_back(vector);
        ghosts.push_back(chicken);
    }
}


