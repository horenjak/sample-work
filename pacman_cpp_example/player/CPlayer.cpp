#include "CPlayer.h"

CPlayer::CPlayer(std::string name, int difficulty, size_t score)
:m_name(std::move(name)),
 m_score(score),
 m_difficulty(difficulty)
{
    m_saveFileName = m_name+"-"+getDifficultyString()+"-"+timeStamp();
}

int CPlayer::getDifficulty() const {
    return m_difficulty;
}

size_t CPlayer::getPlayerScore() const {
    return m_score;
}

std::string CPlayer::getPlayerName() const {
    return m_name;
}

std::string CPlayer::getPersonalFileName() const {
    return m_saveFileName;
}

void CPlayer::setDifficulty(int diff) {
    m_difficulty = diff;
}

void CPlayer::calculatePlayerScore(const CPacman & pacman) {
    m_score += pacman.getScore();
}

std::ostream & operator << (std::ostream & os, const CPlayer & player){
    int numberOfTabs = 3;
    tab(os,numberOfTabs - 2); os << "\"CPlayer\":[" << '\n';
        tab(os,numberOfTabs - 1); os << '{' << '\n';
            tab(os,numberOfTabs); os << "\"name\": " << player.m_name << " ," << '\n';
            tab(os,numberOfTabs); os << "\"difficulty\": " << player.m_difficulty << " ," << '\n';
            tab(os,numberOfTabs); os << "\"fileName\": " << player.m_saveFileName << " ," << '\n';
            tab(os,numberOfTabs); os << "\"score\": " << player.m_score<< " ," << '\n';
        tab(os,numberOfTabs - 1); os << '}' << '\n';
    tab(os,numberOfTabs - 2); os << ']' << '\n';
    return  os;
}

std::istream & propertiesSeparator (std::istream & is, char separator){
    if(separator != ','){
        throw std::invalid_argument ("Wrong separator for Player properties should be separated by \',\'");
    }
    return is;
}

std::istream & operator >> (std::istream & is, CPlayer & player){

    std::string header;
    char separator;

    is >> header;
    if(header == "\"CPlayer\":["){
        is >> separator;
        if(separator == '{'){
            is >> header;
            if(header == "\"name\":"){
                is >> player.m_name >> separator;
            }
            propertiesSeparator(is, separator);
            is >> header;
            if(header == "\"difficulty\":"){
                is >> player.m_difficulty >> separator;
            }
            propertiesSeparator(is, separator);
            is >> header;
            if(header == "\"fileName\":"){
                is >> player.m_saveFileName >> separator;
            }
            propertiesSeparator(is, separator);
            is >> header;
            if(header == "\"score\":"){
                is >> player.m_score >> separator;
            }
            propertiesSeparator(is, separator);
        }else  {
            throw std::invalid_argument ("Wrong separator for Player should be \'{\'");
        }
        is >> separator;
        if(separator != '}'){
            throw std::invalid_argument ("Wrong separator for Player should be \'}\'");
        }
    } else{
        throw std::invalid_argument ("Wrong header for Player");
    }
    is >> separator;
    if(separator != ']'){
        throw std::invalid_argument ("Wrong separator for Player should be \']\'");
    }
    return is;
}

std::string CPlayer::getDifficultyString() {
    switch(m_difficulty){
        case NORMAL:
            return "normal";
        case HARD:
            return "hard";
        case INSANE:
            return "insane";
        case INVINCIBLE:
            return "invincible";
    }
    return "";
}

std::string CPlayer::timeStamp() {
    auto time = std::chrono::system_clock::now();
    std::time_t now_c = std::chrono::system_clock::to_time_t(time);
    std::tm now_tm = *std::localtime(&now_c);

    char buff[80];
    strftime(buff, sizeof buff, "%y:%m:%d:%X", &now_tm);
    std::string timeStr(buff);
    return timeStr;
}
