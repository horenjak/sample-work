#ifndef PACMAN_02_CPLAYER_H
#define PACMAN_02_CPLAYER_H

#include "../EnumsConsts.h"
#include "../UI/pacman/CPacman.h"

#include <chrono>
#include <ctime>
#include <vector>

class CPlayer{
public:
    CPlayer(std::string name, int difficulty = NORMAL, size_t score = 0);
    int getDifficulty() const;
    size_t getPlayerScore() const;
    std::string getPlayerName() const;
    void setDifficulty(int diff);
    std::string getPersonalFileName() const;
    void calculatePlayerScore(const CPacman & pacman);

    friend std::ostream & operator << (std::ostream & os, const CPlayer & player);
    friend std::istream & operator >> (std::istream & is, CPlayer & player);

private:
    std::string m_name;
    std::string m_saveFileName;
    size_t m_score;
    int m_difficulty;

    std::string getDifficultyString();
    std::string timeStamp();

};


#endif //PACMAN_02_CPLAYER_H
