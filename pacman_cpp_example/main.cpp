#include <ncurses.h>
#include <time.h>
#include <vector>

#include "UI/pacman/CPacman.h"
#include "UI/level/CLevel.h"
#include "UI/menu/CMenu.h"
#include "player/CPlayer.h"
#include "save_lode/CSave.h"

using namespace std;

void initGame(){
    srand(time(NULL)); //ok
    initscr();
    noecho(); //hide user input
    curs_set(0); //hide cursor
    timeout(400); // delay set speed of game loop
}
/// this function get input from keyboard
/// \param userInput in this variable stores user input
/// \return true if input is other than ESC
bool getKey(char & userInput){
    userInput = getch();
    if(userInput == ESC){
        return false;
    }
    return true;
}
///poses player game
void pause(){
    char userInput;
    while (getKey(userInput)){
        mvprintw(1,0, "!!!!!!!!!!!!!PAUSED!!!!!!!!!!!!!");
        if(userInput == ' '){
            break;
        }
    }
}
/// every tic runs all necessary methods ghost
/// \param pacman sends information about pacman
/// \param ticCounter send number of tic that happened
/// \param scatterCounter contains count out of ghost scatter
/// \param ghost pointer to ghost to be moved
/// \param hunter ghost vector need position of ghost Hunter to calculate his destination point
void ghostCycle(const CPacman & pacman, size_t ticCounter, int & scatterCounter, CGhost & ghost, const CTile hunter ){

    if (ticCounter % ghost.getSpeed() == 0 && ticCounter >= ghost.getGhostsStaringDelay()) {
        ghost.moveGhost((CMovingTile) pacman, scatterCounter, hunter);
    }

    mvaddch(ghost.getY(), ghost.getX(), ghost.getContent());
}

/// runs level
/// \param player contains player information
/// \param pacman contains pacman information
/// \param ghosts contains information about all ghost in level
/// \param level contains level information
/// \return true if level was successfully finished and false if it was ended by player
bool playLevel(CPlayer & player, CPacman & pacman, std::vector<std::shared_ptr<CGhost>> & ghosts , CLevel & level)
{
    CMenu menu;
    size_t ticCounter = 1;
    bool end = false;
    int scatterCounter = 0;
    char userInput;

    while(!end){
        if(!getKey(userInput)){
            end = menu.displayMenu(player, pacman, ghosts, level);
        } else if( userInput == ' '){
            pause();
        }
        else{
            erase();
            level.drawLevel();
            pacman.movePacman(userInput);
            if(ticCounter % 100 == 0){
                scatterCounter = 30;
            }
            for(auto & ghost: ghosts){
                ghostCycle(pacman, ticCounter, scatterCounter, *ghost, *ghosts[0]);
            }
            pacman.checkPacmanGhostCollision(ghosts, ticCounter);
            if(pacman.pacmanDead() == true){
                pacman = CPacman(level.getRoads());
                ticCounter = 0;
            }
            if(pacman.checkIfLevelIsFinished()){
                return true;
            }
            ticCounter++;
            refresh();
        }
    }
    return false;
}

int main() {
    initscr();
    initGame();

    CSave save;
    CLevel level;
    CMenu menus;
    CPlayer player("empty");

    CPacman pacman(level.getRoads());
    std::vector<std::shared_ptr<CGhost>> ghosts;

    menus.initialScreen(player,pacman,ghosts, level);

    while(playLevel(player, pacman, ghosts, level)){
        player.calculatePlayerScore(pacman);
        ++level;
        save.saveGame(pacman,ghosts,player,level);
        if(!menus.displayProgress(player,level)){
            break;
        }
        pacman.nextLevel(level, player.getDifficulty());
        for(auto & ghost : ghosts){
            ghost->resetGhost(ghosts);
        }
    }
    endwin();
    return 0;
}
